#include "window.h"
#include "ui_window.h"

#include "widgets/oscillator_widget.h"
#include "widgets/noise_widget.h"
#include "widgets/lfo_widget.h"
#include "widgets/envelope_widget.h"
#include "widgets/low_pass_widget.h"
#include "widgets/multiplier_widget.h"
#include "widgets/adder_widget.h"
#include "widgets/cross_fade_widget.h"
#include "widgets/variable_widget.h"
#include "widgets/keyboard_widget.h"
#include "widgets/speaker_widget.h"
#include "widgets/midi_widget.h"
#include "widgets/visualizer_widget.h"

#include <QMessageBox>

Window::Window(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Window),
    mSampleRate(44100)
{
    ui->setupUi(this);

    // Attach a listener to the tree widget
    connect(ui->treeWidget, &QTreeWidget::itemDoubleClicked, [this](QTreeWidgetItem* item, int col)
    {
        addSynthComponent(item->text(col));
    });

    // Set up the global property editors
    connect(ui->sampleRate, QOverload<const QString&>::of(&QComboBox::currentIndexChanged),
        [=](const QString& text)
    {
        mSampleRate = text.toInt();
        emit onSampleRateChanged(mSampleRate);
    });

    connect(ui->cableTension, &QAbstractSlider::valueChanged, [=](int value)
    {
        emit onCableTensionChanged((double) value / (double) ui->cableTension->maximum());
    });

    connect(ui->cableOpacity, &QAbstractSlider::valueChanged, [=](int value)
    {
        emit onCableOpacityChanged((double) value / (double) ui->cableOpacity->maximum());
    });

    // Set up the component frame
    mComponentFrame = new ComponentFrame();
    mComponentFrame->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    mComponentFrame->setWindow(this);
    ui->scrollArea->setWidget(mComponentFrame);
}

Window::~Window()
{
    delete ui;
}

void Window::addSynthComponent(const QString& text)
{
    if (text == "Oscillator")
    {
        mComponentFrame->addChild(new OscillatorWidget(mSampleRate));
    }
    else if (text == "Noise")
    {
        mComponentFrame->addChild(new NoiseWidget());
    }
    else if (text == "LFO")
    {
        mComponentFrame->addChild(new LFOWidget(mSampleRate));
    }
    else if (text == "Low-pass")
    {
        mComponentFrame->addChild(new LowPassWidget(mSampleRate));
    }
    else if (text == "ADSR")
    {
        mComponentFrame->addChild(new EnvelopeWidget(mSampleRate));
    }
    else if (text == "Multiply")
    {
        mComponentFrame->addChild(new MultiplierWidget());
    }
    else if (text == "Cross Fade")
    {
        mComponentFrame->addChild(new CrossFadeWidget());
    }
    else if (text == "Add")
    {
        mComponentFrame->addChild(new AdderWidget());
    }
    else if (text == "Keyboard")
    {
        mComponentFrame->addChild(new KeyboardWidget());
    }
    else if (text == "Line In")
    {

    }
    else if (text == "MIDI")
    {
        mComponentFrame->addChild(new MidiWidget());
    }
    else if (text == "Line Out")
    {
        mComponentFrame->addChild(new SpeakerWidget(mSampleRate));
    }
    else if (text == "Variable")
    {
        mComponentFrame->addChild(new VariableWidget(0.0));
    }
    else if (text == "Visualizer")
    {
        mComponentFrame->addChild(new VisualizerWidget(mSampleRate));
    }
    else
    {
        QMessageBox::critical(this, "Synthesizer", "Unknown Component: " + text);
    }
}
