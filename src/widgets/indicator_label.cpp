#include "widgets/indicator_label.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPainter>

IndicatorLabel::IndicatorLabel(int direction, QWidget *parent) :
    QWidget(parent)
{
    mLabel     = new QLabel();
    mIndicator = new Indicator();

    // Wrap the light in a layout so it can be as small as it wants to be
    QVBoxLayout* lightBox = new QVBoxLayout();
    lightBox->addWidget(mIndicator);

    QHBoxLayout* layout = new QHBoxLayout();
    layout->setMargin(0);

    if (direction == DIRECTION_LEFT)
    {
        layout->addLayout(lightBox);
        layout->addWidget(mLabel);
    }
    else
    {
        layout->addWidget(mLabel);
        layout->addLayout(lightBox);
    }

    setContentsMargins(0, 0, 0, 0);
    setLayout(layout);
}

double IndicatorLabel::brightness() const
{
    return mIndicator->brightness();
}

QColor IndicatorLabel::color() const
{
    return mIndicator->color();
}

QString IndicatorLabel::text() const
{
    return mLabel->text();
}

void IndicatorLabel::setBrightness(double value)
{
    mIndicator->setBrightness(value);
    repaint();
}

void IndicatorLabel::setColor(QColor color)
{
    mIndicator->setColor(color);
    repaint();
}

void IndicatorLabel::setText(QString text)
{
    mLabel->setText(text);
}

void IndicatorLabel::setAlignment(Qt::Alignment align)
{
    mLabel->setAlignment(align);
}

IndicatorLabel::Indicator::Indicator(QWidget *parent) :
    QWidget(parent),
    mColor(Qt::white),
    mBrightness(1.0)
{
    mColor = mColor.toHsv();
    setMinimumSize(20, 20);
    setMaximumSize(20, 20);
}

void IndicatorLabel::Indicator::setColor(QColor color)
{
    mColor = color.toHsv();
    repaint();
}

void IndicatorLabel::Indicator::setBrightness(double brightness)
{
    mBrightness = brightness;
    repaint();
}

QColor IndicatorLabel::Indicator::color() const
{
    return mColor.toRgb();
}

double IndicatorLabel::Indicator::brightness() const
{
    return mBrightness;
}

void IndicatorLabel::Indicator::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    // Set the base color
    double hue, sat, value;
    mColor.getHsvF(&hue, &sat, &value);
    QColor color = QColor::fromHsvF(hue, sat, value * mBrightness);

    // Set up the gradient
    double radius = 0.75 * sqrt(width() * width() + height() * height());
    QRadialGradient fade(width()/2.0, height()/2.0, radius);
    fade.setColorAt(0, color);
    fade.setColorAt(1, Qt::black);

    // Draw the circle
    painter.setPen(Qt::NoPen);
    painter.setBrush(QBrush(fade));
    painter.drawEllipse(1, 1, width() - 2, height() - 2);
}
