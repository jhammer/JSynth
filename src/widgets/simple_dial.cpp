#include "widgets/simple_dial.h"

#include <QVBoxLayout>

SimpleDial::SimpleDial(int minValue, int maxValue, const QString& text, QWidget *parent) :
    QWidget(parent),
    mDefaultValue(0)
{
    mDial = new QDial();
    mDial->setRange(minValue, maxValue);
    mDial->setNotchesVisible(true);

    // Adjust the notch spacing when the range is large
    if (maxValue - minValue + 1 > 200)
        mDial->setNotchTarget(100.0);

    mLabel = new QLabel(text);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->setSpacing(0);
    layout->setContentsMargins(5, 5, 5, 5);
    layout->addWidget(mDial, 1);
    layout->addWidget(mLabel, 0, Qt::AlignTop | Qt::AlignHCenter);
    setLayout(layout);

    connect(mDial, SIGNAL(valueChanged(int)), this, SIGNAL(onValueChanged(int)));
}

void SimpleDial::mousePressEvent(QMouseEvent *event)
{
    // Reset on right click
    if (event->button() == Qt::RightButton)
        mDial->setValue(mDefaultValue);

    QWidget::mousePressEvent(event);
}

void SimpleDial::contextMenuEvent(QContextMenuEvent *event)
{
    // Eat this event so parents don't receive it. (When they
    // right click on this widget, we don't want contextMenuEvent
    // to be propagated.)
    event->accept();
}

void SimpleDial::setValue(int value)
{
    mDial->setValue(value);
}

void SimpleDial::setDefaultValue(int value)
{
    mDefaultValue = value;
}
