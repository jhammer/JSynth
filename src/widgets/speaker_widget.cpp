#include "widgets/speaker_widget.h"
#include "synth/components/pass_through.h"
#include "window.h"
#include <QList>
#include <QAudioDeviceInfo>
#include <QComboBox>
#include <QMessageBox>
#include <QtMultimedia/QAudioDeviceInfo>
#include <QtMultimedia/QAudioOutput>
#include <QApplication>

SpeakerWidget::SpeakerWidget(uint32_t sampleRate, QWidget* parent) :
    ComponentWidget(new synth::pass_through(), parent),
    mSampleRate(sampleRate),
    mGenerator(nullptr),
    mSpeaker(nullptr)
{
    setName("Line Out");
    setInputNames({"Signal"});
    setInputColors({ComponentWidget::COLOR_SOUND});

    // Fill a combo box with all available output audio devices
    QAudioFormat preferred = getPreferredAudioFormat(sampleRate);
    QList<QAudioDeviceInfo> devices = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
    mAudioDevicesBox = new QComboBox();
    mAudioDevicesBox->addItem("None");
    for (QAudioDeviceInfo& info : devices)
    {
        if (!info.isNull() && info.isFormatSupported(preferred))
            mAudioDevicesBox->addItem(info.deviceName());
    }

    // Fill another combo box with all available block size options
    mBlockSizeBox = new QComboBox();
    mBlockSizeBox->insertItems(0, {"16", "32", "64", "128", "256", "512", "1024", "2048", "4096", "8192", "16384"});
    mBlockSizeBox->setCurrentIndex(mBlockSizeBox->count() - 3);

    // Put the layout together
    QGridLayout* layout = new QGridLayout();
    layout->addWidget(new QLabel("Device: "), 0, 0);
    layout->addWidget(mAudioDevicesBox, 0, 1);
    layout->addWidget(new QLabel("Block Size: "), 1, 0);
    layout->addWidget(mBlockSizeBox, 1, 1);

    QWidget* contentPane = new QWidget();
    contentPane->setLayout(layout);
    setContentPane(contentPane);

    // Connect a listener to the devices box so we can change the audio properties
    connect(mAudioDevicesBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index)
    {
        stopAudio();

        if (index != 0)
        {
            // Get the current block size and start the audio
            uint32_t blockSize = mBlockSizeBox->currentText().toInt();
            initAudio(mSampleRate, blockSize);
        }
    });

    // Same for the block size
    connect(mBlockSizeBox, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int)
    {
        stopAudio();
        uint32_t blockSize = mBlockSizeBox->currentText().toInt();
        initAudio(mSampleRate, blockSize);
    });

    Window* window = static_cast<Window*>(QApplication::activeWindow());
    connect(window, &Window::onSampleRateChanged, [=](int sampleRate)
    {
        stopAudio();
        mSampleRate = sampleRate;
        uint32_t blockSize = mBlockSizeBox->currentText().toInt();
        initAudio(mSampleRate, blockSize);
    });
}

SpeakerWidget::~SpeakerWidget()
{
    stopAudio();
}

void SpeakerWidget::initAudio(uint32_t sampleRate, uint32_t blockSize)
{
    QAudioFormat format = getPreferredAudioFormat(sampleRate);
    int index = mAudioDevicesBox->currentIndex() - 1;
    if (index >= 0)
    {
        QList<QAudioDeviceInfo> devices = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
        QAudioDeviceInfo info = devices.at(index);

        if (!info.isFormatSupported(format))
        {
            QMessageBox::critical(this, "JSynth", "Raw audio format not supported by backend. Cannot play audio.");
            mAudioDevicesBox->setCurrentIndex(0);
            return;
        }

        mSpeaker = new QAudioOutput(info, format, this);
        mSpeaker->setBufferSize(blockSize);

        // Generate data every 1 ms or any time the buffer runs out
        mSpeaker->setNotifyInterval(1);
        connect(mSpeaker, &QAudioOutput::notify, this, &SpeakerWidget::generateData);
        connect(mSpeaker, &QAudioOutput::stateChanged, [=](QAudio::State newState)
        {
            if (newState == QAudio::IdleState)
                generateData();
        });

        // Start the speaker
        mGenerator = mSpeaker->start();
        generateData();
    }
}

void SpeakerWidget::stopAudio()
{
    if (mSpeaker != nullptr)
    {
        disconnect(mSpeaker, &QAudioOutput::notify, this, &SpeakerWidget::generateData);
        if (mGenerator != nullptr) mGenerator->close();
        mGenerator = nullptr;

        mSpeaker->stop();
        delete mSpeaker;
        mSpeaker = nullptr;
    }
}

void SpeakerWidget::generateData()
{
    if (mSpeaker == nullptr || mGenerator == nullptr)
        return;

    const int emptyBytes = mSpeaker->bytesFree();
    if (emptyBytes > 0)
    {
        synth::component::endpoint end(getComponent(), 0);
        size_t samples = emptyBytes / sizeof(int16_t);

        static int16_t data[10000000];
        for (size_t i = 0; i < samples; ++i)
            data[i] = (int16_t)(32767 * end());

        mGenerator->write((char*) data, sizeof(int16_t) * samples);
    }
}

QAudioFormat SpeakerWidget::getPreferredAudioFormat(uint32_t sampleRate) const
{
    QAudioFormat format;
    format.setSampleRate(sampleRate);
    format.setChannelCount(1);
    format.setSampleSize(16);
    format.setCodec("audio/pcm");
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setSampleType(QAudioFormat::SignedInt);
    return format;
}
