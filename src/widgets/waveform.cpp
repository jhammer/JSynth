#include "widgets/waveform.h"
#include <QPainter>
#include <QRectF>
#include <QPen>

Waveform::Waveform(size_t sampleRate, double seconds, size_t resolution, QWidget *parent) :
    QWidget(parent),
    mBuffer(new QPointF[(size_t)(sampleRate * seconds) / resolution]()),
    mBufferSize((size_t)(sampleRate * seconds) / resolution),
    mCurIndex(0),
    mDurationSeconds(seconds),
    mResolution(resolution),
    mSingleCounter(0),
    mWindowStart(0.0),
    mWindowEnd(0.0)
{
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    for (size_t i = 0; i < mBufferSize; ++i)
    {
        mBuffer[i].setX((double) i / (double) mBufferSize);
        mBuffer[i].setY(0.5);
    }
    mWindowEnd = 1.0;
}

Waveform::~Waveform()
{
    delete[] mBuffer;
}

void Waveform::write(float *samples, size_t count)
{
    const double xInc = 1.0 / mBufferSize;
    for (size_t i = 0; i < count / mResolution; ++i)
    {
        mBuffer[mCurIndex].setX(mWindowEnd);
        mBuffer[mCurIndex].setY((samples[mResolution * i] + 1.0) * 0.5);
        ++mCurIndex;

        if (mCurIndex >= mBufferSize)
            mCurIndex = 0;

        mWindowEnd += xInc;
    }

    // Move the window forward
    mWindowStart += (count / mResolution) * xInc;

    this->repaint();
}

void Waveform::write(float sample)
{
    // Only process 1 sample in 'mResoluion'
    if (mSingleCounter >= mResolution)
    {
        const double xInc = 1.0 / mBufferSize;

        mBuffer[mCurIndex].setX(mWindowEnd);
        mBuffer[mCurIndex].setY((sample + 1.0) * 0.5);
        ++mCurIndex;

        if (mCurIndex >= mBufferSize)
            mCurIndex = 0;

        // Move the window forward
        mWindowStart += xInc;
        mWindowEnd   += xInc;

        this->repaint();
        mSingleCounter = 0;
    }

    mSingleCounter++;
}

void Waveform::setResolution(size_t resolution)
{
    mResolution = resolution;
}

void Waveform::setSampleRate(size_t sampleRate)
{
    delete[] mBuffer;

    mBufferSize    = (size_t) sampleRate * mDurationSeconds / mResolution;
    mBuffer        = new QPointF[mBufferSize]();
    mCurIndex      = 0;
    mSingleCounter = 0;
    mWindowStart   = 0.0;
    mWindowEnd     = 0.0;

    for (size_t i = 0; i < mBufferSize; ++i)
    {
        mBuffer[i].setX((double) i / (double) mBufferSize);
        mBuffer[i].setY(0.5);
    }
    mWindowEnd = 1.0;
    repaint();
}

void Waveform::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter painter(this);

    painter.scale(event->rect().width(), event->rect().height());
    painter.translate(-mWindowStart, 0.0);

    QPen pen = painter.pen();
    //pen.setCosmetic(true);
    pen.setWidth(0);
    pen.setColor(QColor(32, 159, 223));
    painter.setPen(pen);

    painter.drawPolyline(mBuffer + mCurIndex, mBufferSize - mCurIndex);

    if (mCurIndex > 0)
    {
        painter.drawLine(mBuffer[mBufferSize - 1], mBuffer[0]);
        painter.drawPolyline(mBuffer, mCurIndex);
    }
}
