#include <QApplication>
#include "widgets/oscillator_widget.h"
#include "widgets/simple_dial.h"
#include "synth/components/oscillator.h"
#include "window.h"

OscillatorWidget::OscillatorWidget(uint32_t sampleRate, QWidget* parent) :
    ComponentWidget(new synth::oscillator(sampleRate), parent)
{
    setName("Oscillator");
    setInputNames({"Freq", "Amp"});
    setOutputNames({"Out"});
    setInputColors({ComponentWidget::COLOR_FREQ, ComponentWidget::COLOR_CONTROL});
    setOutputColors({ComponentWidget::COLOR_SOUND});

    SimpleDial* waveform = new SimpleDial(0, 3, "Waveform");
    SimpleDial* octave   = new SimpleDial(-4, 4, "Octave");
    SimpleDial* semi     = new SimpleDial(-12, 12, "Semi");
    SimpleDial* fine     = new SimpleDial(-50, 50, "Fine");

    QVBoxLayout* layout = new QVBoxLayout();
    layout->setSpacing(0);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(waveform, 1);

    QHBoxLayout* pitch = new QHBoxLayout();
    pitch->setSpacing(0);
    pitch->setContentsMargins(0, 0, 0, 0);
    pitch->addWidget(octave);
    pitch->addWidget(semi);
    pitch->addWidget(fine);
    layout->addLayout(pitch, 1);

    QWidget* contentPane = new QWidget();
    contentPane->setLayout(layout);
    setContentPane(contentPane);

    connect(waveform, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
    {
        synth::oscillator* comp = (synth::oscillator*) getComponent();
        switch (val)
        {
            case 0: comp->waveform(synth::oscillator::waveform_type::SIN);      break;
            case 1: comp->waveform(synth::oscillator::waveform_type::TRIANGLE); break;
            case 2: comp->waveform(synth::oscillator::waveform_type::SAW);      break;
            case 3: comp->waveform(synth::oscillator::waveform_type::SQUARE);   break;
        }
    });

    connect(octave, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
    {
        synth::oscillator* comp = (synth::oscillator*) getComponent();
        comp->setOctaveOffset(val);
    });
    connect(semi, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
    {
        synth::oscillator* comp = (synth::oscillator*) getComponent();
        comp->setSemitoneOffset(val);
    });
    connect(fine, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
    {
        synth::oscillator* comp = (synth::oscillator*) getComponent();
        comp->setFineOffset(val);
    });


    Window* window = static_cast<Window*>(QApplication::activeWindow());
    connect(window, &Window::onSampleRateChanged, [=](int sampleRate)
    {
        ((synth::oscillator*) getComponent())->sampleRate(sampleRate);
    });
}
