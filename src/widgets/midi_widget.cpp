#include "widgets/midi_widget.h"
#include "synth/components/keyboard.h"

void midiCallback( double /*deltatime*/, std::vector< unsigned char > *message, void *userData )
{
    if (message->size() > 0)
    {
        MidiWidget* parent = (MidiWidget*) userData;

        switch(message->at(0) & 0xF0)
        {
            // Note ON
            case 144:
            {
                const int channel  = message->at(0) & 0x0F;
                const int note     = message->at(1);
                const int pressure = message->at(2);
                parent->noteOn(note, pressure, channel);
            }
            break;

            // Note OFF
            case 128:
            {
                const int channel  = message->at(0) & 0x0F;
                const int note     = message->at(1);
                parent->noteOff(note, channel);
            }
            break;
        }
    }
}

MidiWidget::MidiWidget(QWidget* parent) :
    ComponentWidget(new synth::keyboard(), parent),
    mInput(nullptr)
{
    setName("Midi");
    setOutputNames({"Freq", "Key Down"});
    setOutputColors({ComponentWidget::COLOR_FREQ, ComponentWidget::COLOR_CONTROL});

    // Fill a combo box with all available MIDI devices
    mMidiDevices = new QComboBox();
    mMidiDevices->addItem("None");
    try
    {
        mInput = new RtMidiIn();
        for (size_t i = 0; i < mInput->getPortCount(); ++i)
            mMidiDevices->addItem(mInput->getPortName(i).c_str());
    }
    catch (RtMidiError &error)
    {
        error.printMessage();
    }

    // Put the layout together
    QGridLayout* layout = new QGridLayout();
    layout->addWidget(new QLabel("Device: "), 0, 0);
    layout->addWidget(mMidiDevices, 0, 1);

    QWidget* contentPane = new QWidget();
    contentPane->setLayout(layout);
    setContentPane(contentPane);

    // Connect a listener to the devices box so we can adjust the midi source
    connect(mMidiDevices, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index)
    {
        mInput->closePort();
        mInput->cancelCallback();

        if (index > 0)
        {
            mInput->openPort(index - 1);
            mInput->setCallback( &midiCallback, this );
        }
    });
}

MidiWidget::~MidiWidget()
{
    delete mInput;
}

void MidiWidget::noteOn(int note, int pressure, int channel)
{
    synth::keyboard* comp = (synth::keyboard*) getComponent();
    comp->key_down(note);
}

void MidiWidget::noteOff(int note, int channel)
{
    synth::keyboard* comp = (synth::keyboard*) getComponent();
    comp->key_up(note);
}
