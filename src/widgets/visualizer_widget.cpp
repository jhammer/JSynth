#include "widgets/visualizer_widget.h"
#include "window.h"
#include <QApplication>

VisualizerWidget::VisualizerWidget(uint32_t sampleRate, QWidget* parent) :
    ComponentWidget(new visualizer_component(this), parent)
{
    setName("Waveform");
    setInputNames({"In"});
    setOutputNames({"Out"});
    setInputColors({ComponentWidget::COLOR_SOUND});
    setOutputColors({ComponentWidget::COLOR_SOUND});

    // Set up the viewing window
    mWaveform = new Waveform(sampleRate, 0.5, 128);
    QGridLayout* frameLayout = new QGridLayout();
    frameLayout->setContentsMargins(0, 5, 0, 5);
    frameLayout->addWidget(mWaveform);

    // Set up the frame around the viewing window
    QFrame* frame = new QFrame();
    frame->setFrameStyle(QFrame::Box);
    frame->setMinimumWidth(200);
    frame->setMinimumHeight(100);
    frame->setLayout(frameLayout);

    // Set up the content pane
    QGridLayout* layout = new QGridLayout();
    layout->addWidget(frame);

    QWidget* contentPane = new QWidget();
    contentPane->setLayout(layout);
    setContentPane(contentPane);

    Window* window = static_cast<Window*>(QApplication::activeWindow());
    connect(window, &Window::onSampleRateChanged, [=](int sampleRate)
    {
        mWaveform->setSampleRate(sampleRate);
    });
}
