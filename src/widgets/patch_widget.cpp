#include "widgets/patch_widget.h"
#include <QPainter>

PatchWidget::PatchWidget(QWidget *parent) :
    QWidget(parent),
    mSource(nullptr),
    mDest(nullptr),
    mSourcePort(-1),
    mDestPort(-1),
    mEndpoint(nullptr),
    mColor(Qt::black),
    mConnectionTension(100.0)
{
    setAttribute(Qt::WA_TransparentForMouseEvents);
}

PatchWidget::~PatchWidget()
{
    delete mEndpoint;
    mEndpoint = nullptr;
}

void PatchWidget::setSource(ComponentWidget *source, int sourcePort)
{
    mSource     = source;
    mSourcePort = sourcePort;
}

void PatchWidget::setDest(ComponentWidget *dest, int destPort)
{
    mDest     = dest;
    mDestPort = destPort;
}

void PatchWidget::setColor(QColor color)
{
    mColor = color;
    repaint();
}

void PatchWidget::setConnectionTension(double tension)
{
    mConnectionTension = tension;
    repaint();
}

void PatchWidget::setEndpoint(QPoint endpoint)
{
    delete mEndpoint;
    mEndpoint = new QPoint(endpoint);
}

void PatchWidget::removeEndpoint()
{
    delete mEndpoint;
    mEndpoint = nullptr;
}

ComponentWidget* PatchWidget::source() const
{
    return mSource;
}

ComponentWidget* PatchWidget::dest() const
{
    return mDest;
}

int PatchWidget::sourcePort() const
{
    return mSourcePort;
}

int PatchWidget::destPort() const
{
    return mDestPort;
}

QColor PatchWidget::color() const
{
    return mColor;
}

double PatchWidget::connectionTension() const
{
    return mConnectionTension;
}

// Draws a smooth parabola between A and B. 'tension' controls
// the extent to which it droops in pixels.
void arc(QPainter& painter, QPoint A, QPoint B, double tension,
         QColor color, double outsideThickness = 5.0, double insideThickness = 3.0)
{
    QPointF C = 0.5 * A + 0.5 * B;
    C.setY(C.y() + tension);

    QPen pen;
    pen.setWidth(outsideThickness);
    pen.setColor(Qt::black);
    pen.setCapStyle(Qt::RoundCap);
    painter.setPen(pen);

    QPainterPath path;
    path.moveTo(A);
    path.quadTo(C, B);
    painter.drawPath(path);

    pen.setWidth(insideThickness);
    pen.setColor(color);
    painter.setPen(pen);
    painter.drawPath(path);
}

void PatchWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    const double outerThickness = 8.0;
    const double innerThickness = 5.0;

    if (mSource != nullptr && mDest != nullptr)
    {
        // Draw the arc
        QPoint start = mSource->pos() + mSource->outputOffset(mSourcePort);
        QPoint end   = mDest->pos()   + mDest->inputOffset(mDestPort);
        arc(painter, start, end, mConnectionTension, mColor, outerThickness, innerThickness);
    }
    else if (mSource != nullptr && mEndpoint != nullptr)
    {
        QPoint start = mSource->pos() + mSource->outputOffset(mSourcePort);
        arc(painter, start, *mEndpoint, mConnectionTension, mColor, outerThickness, innerThickness);
    }
    else if (mSource == nullptr && mEndpoint != nullptr)
    {
        QPoint end = mDest->pos() + mDest->inputOffset(mDestPort);
        arc(painter, *mEndpoint, end, mConnectionTension, mColor, outerThickness, innerThickness);
    }
}
