#include "widgets/component_widget.h"
#include "widgets/indicator_label.h"
#include "window.h"

const QColor ComponentWidget::COLOR_SOUND   = QColor(63, 72, 204);
const QColor ComponentWidget::COLOR_CONTROL = Qt::red;
const QColor ComponentWidget::COLOR_FREQ    = Qt::green;
const QColor ComponentWidget::COLOR_ANY     = Qt::white;

ComponentWidget::ComponentWidget(synth::component* component, QWidget *parent) :
    QFrame(parent),
    mComponent(component)
{
    setObjectName("ComponentWidget");

    // Apply ComponentWidget-specific styling
    QString style =
        /*"text-transform: uppercase; " \ */
        "background-color: rgb(200, 200, 200); " \
        "font-size: 9pt; "                       \
        "font-weight: 900; "                     \
        "color: rgb(0,0,0);"                     \
        "border-width: 3px";
    this->setStyleSheet(style);

    // Key pieces
    mGrid          = new QGridLayout();
    mInputsLayout  = new QVBoxLayout();
    mOutputsLayout = new QVBoxLayout();
    mNameLabel     = new QLabel("Name");

    QWidget* placeholder = new QWidget();
    placeholder->setMinimumSize(100, 100);

    // Children elements
    mGrid->addWidget(mNameLabel, 0, 0, 1, 3, Qt::AlignCenter);
    mGrid->addLayout(mInputsLayout, 1, 0);
    mGrid->addWidget(placeholder, 1, 1);
    mGrid->addLayout(mOutputsLayout, 1, 2);

    // Layout properties
    mGrid->setColumnStretch(0, 0);
    mGrid->setColumnStretch(1, 1);
    mGrid->setColumnStretch(2, 0);
    mGrid->setRowStretch(0, 0);
    mGrid->setRowStretch(1, 1);
    mGrid->setSpacing(0);
    mGrid->setContentsMargins(5, 0, 5, 0);

    setFrameShape(QFrame::Box);
    setLayout(mGrid);
}

ComponentWidget::~ComponentWidget()
{
    if (mComponent != nullptr)
        delete mComponent;
}

void ComponentWidget::setName(const QString &name)
{
    mNameLabel->setText(name);
}

void ComponentWidget::setInputNames(const QStringList& inputNames)
{
    mInputLabels.clear();
    while (!mInputsLayout->isEmpty())
        delete mInputsLayout->takeAt(0);

    for (const QString& in : inputNames)
    {
        IndicatorLabel* label = new IndicatorLabel(IndicatorLabel::DIRECTION_LEFT);
        label->setText(in);
        mInputsLayout->addWidget(label , Qt::AlignLeft | Qt::AlignVCenter);
        mInputLabels.push_back(label);
    }
}

void ComponentWidget::setOutputNames(const QStringList& outputNames)
{
    mOutputLabels.clear();
    while (!mOutputsLayout->isEmpty())
        delete mOutputsLayout->takeAt(0);

    for (const QString& out : outputNames)
    {
        IndicatorLabel* label = new IndicatorLabel(IndicatorLabel::DIRECTION_RIGHT);
        label->setText(out);
        label->show();
        label->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
        mOutputsLayout->addWidget(label, Qt::AlignRight | Qt::AlignVCenter);
        mOutputLabels.push_back(label);
    }
}

void ComponentWidget::setInputColors(const std::vector<QColor> inputColors)
{
    for (int i = 0; i < mInputLabels.count(); ++i)
    {
        if (i < (int) inputColors.size())
            mInputLabels[i]->setColor(inputColors[i]);
        else mInputLabels[i]->setColor(COLOR_ANY);
    }
    repaint();
}

void ComponentWidget::setOutputColors(const std::vector<QColor> outputColors)
{
    for (int i = 0; i < mOutputLabels.count(); ++i)
    {
        if (i < (int) outputColors.size())
            mOutputLabels[i]->setColor(outputColors[i]);
        else mOutputLabels[i]->setColor(COLOR_ANY);
    }
    repaint();
}

void ComponentWidget::setContentPane(QWidget* contentPane)
{
    mGrid->addWidget(contentPane, 1, 1, Qt::AlignCenter);
}

QRect ComponentWidget::getNameRect() const
{
    QPoint topLeft     = mNameLabel->mapTo(this, QPoint(0, 0));
    QPoint bottomRight = mNameLabel->mapTo(this, QPoint(mNameLabel->width(), mNameLabel->height()));
    return QRect(topLeft, bottomRight);
}

int ComponentWidget::inputIndex(const QPoint &pos) const
{
    for (int i = 0; i < mInputLabels.count(); ++i)
    {
        if (mInputLabels[i]->geometry().contains(pos))
            return i;
    }
    return -1;
}

int ComponentWidget::outputIndex(const QPoint &pos) const
{
    for (int i = 0; i < mOutputLabels.count(); ++i)
    {
        if (mOutputLabels[i]->geometry().contains(pos))
            return i;
    }
    return -1;
}

QPoint ComponentWidget::inputOffset(const int index) const
{
    if (index >= 0 && index < mInputLabels.size())
    {
        IndicatorLabel* label = mInputLabels[index];
        int offset = 9; //contentsMargins().left() + lineWidth() + 1;
        QPoint ret = label->rect().topLeft();
        return label->mapTo(this, ret + QPoint(offset, label->height() / 2));
    }
    else return QPoint();
}

QPoint ComponentWidget::outputOffset(const int index) const
{
    if (index >= 0 && index < mOutputLabels.size())
    {
        IndicatorLabel* label = mOutputLabels[index];
        int offset = 9; //contentsMargins().right() + lineWidth() + 1;
        QPoint ret = label->rect().topRight();
        return label->mapTo(this, ret + QPoint(-offset, label->height() / 2));
    }
    else return QPoint();
}

synth::component* ComponentWidget::getComponent() const
{
    return mComponent;
}

QColor ComponentWidget::inputColor(const int index) const
{
    if (index >= 0 && index < mInputLabels.size())
        return mInputLabels[index]->color();
    else return QColor();
}

QColor ComponentWidget::outputColor(const int index) const
{
    if (index >= 0 && index < mOutputLabels.size())
        return mOutputLabels[index]->color();
    else return QColor();
}
