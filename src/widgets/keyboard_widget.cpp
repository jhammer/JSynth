#include "widgets/keyboard_widget.h"
#include "widgets/simple_dial.h"
#include "synth/components/keyboard.h"

KeyboardWidget::KeyboardWidget(QWidget* parent) :
    ComponentWidget(new synth::keyboard(), parent)
{
    setName("Keyboard");
    setOutputNames({"Freq", "Key Down"});
    setOutputColors({ComponentWidget::COLOR_FREQ, ComponentWidget::COLOR_CONTROL});

    SimpleDial* octave = new SimpleDial(-4, 4, "Octave");

    QGridLayout* layout = new QGridLayout();
    layout->addWidget(octave);

    QWidget* contentPane = new QWidget();
    contentPane->setLayout(layout);
    setContentPane(contentPane);

    connect(octave, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
    {
        synth::keyboard* comp = (synth::keyboard*) getComponent();
        comp->set_octave_offset(val);
    });

    setFocusPolicy(Qt::StrongFocus);
}

void KeyboardWidget::keyPressEvent(QKeyEvent *event)
{
    synth::keyboard* comp = (synth::keyboard*) getComponent();
    switch(event->key())
    {
        case Qt::Key_Z: comp->key_down(60); break;
        case Qt::Key_S: comp->key_down(61); break;
        case Qt::Key_X: comp->key_down(62); break;
        case Qt::Key_D: comp->key_down(63); break;
        case Qt::Key_C: comp->key_down(64); break;
        case Qt::Key_V: comp->key_down(65); break;
        case Qt::Key_G: comp->key_down(66); break;
        case Qt::Key_B: comp->key_down(67); break;
        case Qt::Key_H: comp->key_down(68); break;
        case Qt::Key_N: comp->key_down(69); break;
        case Qt::Key_J: comp->key_down(70); break;
        case Qt::Key_M: comp->key_down(71); break;
        case Qt::Key_Comma: comp->key_down(72); break;
    }

    QWidget::keyPressEvent(event);
}

void KeyboardWidget::keyReleaseEvent(QKeyEvent *event)
{
    synth::keyboard* comp = (synth::keyboard*) getComponent();
    switch(event->key())
    {
        case Qt::Key_Z: comp->key_up(60); break;
        case Qt::Key_S: comp->key_up(61); break;
        case Qt::Key_X: comp->key_up(62); break;
        case Qt::Key_D: comp->key_up(63); break;
        case Qt::Key_C: comp->key_up(64); break;
        case Qt::Key_V: comp->key_up(65); break;
        case Qt::Key_G: comp->key_up(66); break;
        case Qt::Key_B: comp->key_up(67); break;
        case Qt::Key_H: comp->key_up(68); break;
        case Qt::Key_N: comp->key_up(69); break;
        case Qt::Key_J: comp->key_up(70); break;
        case Qt::Key_M: comp->key_up(71); break;
        case Qt::Key_Comma: comp->key_up(72); break;
    }

    QWidget::keyReleaseEvent(event);
}
