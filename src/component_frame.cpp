#include "component_frame.h"
#include "widgets/component_widget.h"
#include "widgets/patch_widget.h"
#include <QtWidgets>

ComponentFrame::ComponentFrame(QWidget* parent) :
    QWidget(parent),
    mWidgetDragger(this),
    mPatchConnector(this),
    mTempPatch(new PatchWidget(this))
{
    // Set up the temp patch
    mTempPatch->setColor(Qt::yellow);
    mTempPatch->show();
    mTempPatch->setObjectName("TempPatch");
    connect(this, &ComponentFrame::onResize, mTempPatch, QOverload<int, int>::of(&QWidget::resize));
    mTempPatch->resize(width(), height());

    setAcceptDrops(true);
    setMinimumWidth(500);
    setMinimumHeight(500);
}

void ComponentFrame::patch(ComponentWidget *source, int sourcePort,
                           ComponentWidget *dest, int destPort,
                           QColor color, double connectionTension)
{
    // Attach the associated components
    synth::component* srcComp  = source->getComponent();
    synth::component* destComp = dest->getComponent();
    destComp->set_predecessor(destPort, srcComp, sourcePort);

    // Create the patch
    PatchWidget* patch = new PatchWidget(this);
    patch->resize(width(), height());
    patch->show();
    patch->setSource(source, sourcePort);
    patch->setDest(dest, destPort);
    patch->setColor(color);
    patch->setConnectionTension(connectionTension);
    patch->lower();
    connect(this, &ComponentFrame::onResize, patch, QOverload<int, int>::of(&QWidget::resize));
}

void ComponentFrame::removePatch(PatchWidget *patch)
{
    // Detach the associated components
    synth::component* destComp = patch->dest()->getComponent();
    destComp->set_predecessor(patch->destPort(), nullptr, 0);

    // Stop all events
    disconnect(this, &ComponentFrame::onResize, patch, QOverload<int, int>::of(&QWidget::resize));

    // Delete the patch
    patch->setParent(nullptr);
    delete patch;
}

void ComponentFrame::addChild(QWidget *widget)
{
    widget->setParent(this);
    widget->show();
}

void ComponentFrame::removeComponent(ComponentWidget *child)
{
    // Eliminate all patches that rely on this component
    QList<PatchWidget*> children = findChildren<PatchWidget*>();
    for (auto it = children.rbegin(); it != children.rend(); ++it)
    {
        PatchWidget* patch = *it;
        if (patch->source() == child || patch->dest() == child)
            removePatch(patch);
    }

    // Remove the component itself
    child->setParent(nullptr);
    delete child;
}

void ComponentFrame::setWindow(Window *window)
{
    connect(window, &Window::onCableTensionChanged, [=](double value)
    {
        QList<PatchWidget*> children = findChildren<PatchWidget*>();
        for (PatchWidget* child : children)
            child->setConnectionTension(value * 200);
        mTempPatch->setConnectionTension(value * 200);
    });
}

void ComponentFrame::mousePressEvent(QMouseEvent *event)
{
    // Only deal with left mouse clicks
    if (event->button() != Qt::LeftButton) return;

    ComponentWidget* source = componentAt(event->pos());
    if (!source) return;

    // When the user clicks on the name label, drag the widget
    QPoint relLocation = source->mapFromParent(event->pos());
    if (source->getNameRect().contains(relLocation))
        mWidgetDragger.onMousePress(event);

    // When the user clicks on one of the inputs / outputs,
    // start a new patch connection
    else mPatchConnector.onMousePress(event);
}

void ComponentFrame::mouseMoveEvent(QMouseEvent *event)
{
    // TOME: No need to verify correct button was pressed

    if (mWidgetDragger.dragWidget != nullptr)
        mWidgetDragger.onMouseMove(event);
    else mPatchConnector.onMouseMove(event);
}

void ComponentFrame::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() != Qt::LeftButton) return;

    if (mWidgetDragger.dragWidget != nullptr)
        mWidgetDragger.onMouseRelease(event);
    else mPatchConnector.onMouseRelease(event);
}

void ComponentFrame::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);

    // Prevent the frame from shrinking when resized
    if (width() > minimumWidth())
        setMinimumWidth(width());
    if (height() > minimumHeight())
        setMinimumHeight(height());

    // All PatchWidgets will need to be resized as well
    emit onResize(width(), height());
}

void ComponentFrame::contextMenuEvent(QContextMenuEvent *event)
{
    ComponentWidget* source = componentAt(event->pos());
    if (!source) return;

    QMenu menu(this);
    menu.addAction("Delete", [=]()
    {
        removeComponent(source);
    });
    menu.exec(event->globalPos());
}

ComponentWidget* ComponentFrame::componentAt(QPoint pos)
{
    QList<ComponentWidget*> children = findChildren<ComponentWidget*>();
    for (auto it = children.rbegin(); it != children.rend(); ++it)
    {
        ComponentWidget* child = *it;
        if (child->geometry().contains(pos))
            return child;
    }
    return nullptr;
}

void ComponentFrame::repaintAll()
{
    repaint();
    QList<ComponentWidget*> children = findChildren<ComponentWidget*>();
    for (ComponentWidget* child : children)
        child->repaint();
}

bool ComponentFrame::conflictingPatch(ComponentWidget *source, int sourcePort,
                                      ComponentWidget *dest, int destPort)
{
    Q_UNUSED(sourcePort);
    Q_UNUSED(destPort);

    // Conflicting patches meet one of the following conditions:
    //   1. Maps a signal to a dest port that is already occupied
    //   2. Maps a signal within the same widget

    QList<PatchWidget*> children = findChildren<PatchWidget*>();
    for (PatchWidget* child : children)
    {
        // NOTE: We do not want to consider the temp patch
        if (child == mTempPatch) continue;

        if (child->dest() == dest && child->destPort() == destPort)
            return true;
    }
    if (source == dest) return true;

    return false;
}

// ---------------------------------------------------------------------------------------------//
void ComponentFrame::WidgetDragger::onMousePress(QMouseEvent *event)
{
    ComponentWidget* source = parent->componentAt(event->pos());
    if (source == nullptr) return;

    // Keep track of which object is being dragged
    source->raise();
    dragWidget = source;
    hotspot    = event->pos() - source->pos();
    parent->repaintAll();
}

void ComponentFrame::WidgetDragger::onMouseMove(QMouseEvent* event)
{
    if (dragWidget == nullptr) return;

    // Move the widget to its new location
    QPoint location = event->pos() - hotspot;
    dragWidget->move(location);
    parent->repaintAll();
}

void ComponentFrame::WidgetDragger::onMouseRelease(QMouseEvent* event)
{
    Q_UNUSED(event);
    if (dragWidget == nullptr) return;

    // Expand the parent to fully contain the child
    using namespace std;
    const int dragX        = dragWidget->x();
    const int dragY        = dragWidget->y();
    const int dragWidth    = dragWidget->width();
    const int dragHeight   = dragWidget->height();
    const int parentWidth  = parent->width();
    const int parentHeight = parent->height();

    if (dragX < 0)
    {
        const int delta = max(-dragX, 0);
        parent->setMinimumWidth(parent->minimumWidth() + delta);

        // Move all widgets by the delta amount
        QList<ComponentWidget*> children = parent->findChildren<ComponentWidget*>();
        for (ComponentWidget* child : children)
            child->move(child->pos() + QPoint(delta, 0));
    }
    if (dragY < 0)
    {
        const int delta = max(-dragY, 0);
        parent->setMinimumHeight(parent->minimumHeight() + delta);

        // Move all widgets by the delta amount
        QList<ComponentWidget*> children = parent->findChildren<ComponentWidget*>();
        for (ComponentWidget* child : children)
            child->move(child->pos() + QPoint(0, delta));
    }
    if (dragX + dragWidth > parentWidth)
    {
        const int delta = dragX + dragWidth - parentWidth;
        parent->setMinimumWidth(parent->minimumWidth() + delta);
    }
    if (dragY + dragHeight > parentHeight)
    {
        const int delta = dragY + dragHeight - parentHeight;
        parent->setMinimumHeight(parent->minimumHeight() + delta);
    }

    dragWidget = nullptr;
    parent->repaintAll();
}

// ---------------------------------------------------------------------------------------------//

void ComponentFrame::PatchConnector::onMousePress(QMouseEvent *event)
{
    // Work out which component was clicked (if any)
    ComponentWidget* obj = parent->componentAt(event->pos());
    if (!obj) return;

    // Returns the patch that has the given component/port as its source
    auto sourcePatch = [](ComponentFrame* parent, ComponentWidget* obj, int port)
    {
        PatchWidget* affectedPatch = nullptr;
        QList<PatchWidget*> children = parent->findChildren<PatchWidget*>();
        for (PatchWidget* child : children)
        {
            // Ignore the temp patch
            if (child == parent->mTempPatch) continue;

            if (child->source() == obj && child->sourcePort() == port)
            {
                affectedPatch = child;
                break;
            }
        }
        return affectedPatch;
    };
    auto destPatch = [](ComponentFrame* parent, ComponentWidget* obj, int port)
    {
        PatchWidget* affectedPatch = nullptr;
        QList<PatchWidget*> children = parent->findChildren<PatchWidget*>();
        for (PatchWidget* child : children)
        {
            // Ignore the temp patch
            if (child == parent->mTempPatch) continue;

            if (child->dest() == obj && child->destPort() == port)
            {
                affectedPatch = child;
                break;
            }
        }
        return affectedPatch;
    };

    // Did they click on an input or output port?
    QPoint relLocation = obj->mapFromParent(event->pos());
    int inputIndex     = obj->inputIndex(relLocation);
    int outputIndex    = obj->outputIndex(relLocation);

    if (inputIndex != -1)
    {
        // If there's already a patch here, we need to delete it
        // and start a new temp patch
        PatchWidget* affectedPatch = destPatch(parent, obj, inputIndex);
        if (affectedPatch != nullptr)
        {
            // Save the source for the temp patch
            ComponentWidget* src = affectedPatch->source();
            int srcPort          = affectedPatch->sourcePort();

            // Eliminate the original patch
            parent->removePatch(affectedPatch);

            // Set up the temp patch
            parent->mTempPatch->setSource(src, srcPort);
            parent->mTempPatch->setDest(nullptr, 0);
            parent->mTempPatch->setEndpoint(event->pos());
            parent->mTempPatch->setColor(src->outputColor(srcPort));
            parent->mTempPatch->raise();
        }

        // If there's not a patch here, start a new temp patch
        else
        {
            parent->mTempPatch->setSource(nullptr, 0);
            parent->mTempPatch->setDest(obj, inputIndex);
            parent->mTempPatch->setEndpoint(event->pos());
            parent->mTempPatch->setColor(obj->inputColor(inputIndex));
            parent->mTempPatch->raise();
        }
    }
    else if (outputIndex != -1)
    {
        // If there's already a patch here, we need to delete it
        // and start a new temp patch
        PatchWidget* affectedPatch = sourcePatch(parent, obj, outputIndex);
        if (affectedPatch != nullptr)
        {
            // Save the source for the temp patch
            ComponentWidget* dest = affectedPatch->dest();
            int destPort          = affectedPatch->destPort();

            // Eliminate the original patch
            parent->removePatch(affectedPatch);

            // Set up the temp patch
            parent->mTempPatch->setSource(nullptr, 0);
            parent->mTempPatch->setDest(dest, destPort);
            parent->mTempPatch->setEndpoint(event->pos());
            parent->mTempPatch->setColor(dest->inputColor(destPort));
            parent->mTempPatch->raise();
        }

        // If there's not a patch here, start a new temp patch
        else
        {
            parent->mTempPatch->setSource(obj, outputIndex);
            parent->mTempPatch->setDest(nullptr, 0);
            parent->mTempPatch->setEndpoint(event->pos());
            parent->mTempPatch->setColor(obj->outputColor(outputIndex));
            parent->mTempPatch->raise();
        }
    }
}

void ComponentFrame::PatchConnector::onMouseMove(QMouseEvent *event)
{
    if (parent->mTempPatch->source() != nullptr ||
        parent->mTempPatch->dest()   != nullptr)
    {
        parent->mTempPatch->setEndpoint(event->pos());
        parent->repaintAll();
    }
}

void ComponentFrame::PatchConnector::onMouseRelease(QMouseEvent *event)
{
    [=](QMouseEvent* event)
    {
        // If there's no temp patch, there's nothing to be done
        if (parent->mTempPatch->source() == nullptr &&
            parent->mTempPatch->dest()   == nullptr)
            return;

        ComponentWidget* source = parent->componentAt(event->pos());
        if (!source) return;

        // Determine if the mouse position maps to an input/output port
        QPoint relLocation = source->mapFromParent(event->pos());
        int inputIndex     = source->inputIndex(relLocation);
        int outputIndex    = source->outputIndex(relLocation);

        ComponentWidget* tmpSource = parent->mTempPatch->source();
        ComponentWidget* tmpDest   = parent->mTempPatch->dest();
        int tmpSourcePort          = parent->mTempPatch->sourcePort();
        int tmpDestPort            = parent->mTempPatch->destPort();
        QColor tmpColor            = parent->mTempPatch->color();
        double connectionTension   = parent->mTempPatch->connectionTension();

        // Try to make an input patch
        if (tmpSource != nullptr && inputIndex != -1 &&
            !parent->conflictingPatch(tmpSource, tmpSourcePort, source, inputIndex))
            parent->patch(tmpSource, tmpSourcePort, source, inputIndex, tmpColor, connectionTension);

        // Try to make an output patch
        else if (tmpDest != nullptr && outputIndex != -1 &&
             !parent->conflictingPatch(source, outputIndex, tmpDest, tmpDestPort))
              parent->patch(source, outputIndex, tmpDest, tmpDestPort, tmpColor, connectionTension);
    }(event);

    // Cleanup
    parent->mTempPatch->setSource(nullptr, 0);
    parent->mTempPatch->setDest(nullptr, 0);
    parent->mTempPatch->removeEndpoint();
    parent->repaintAll();
}
