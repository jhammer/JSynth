#include "window.h"
#include <QApplication>
#include <QTextStream>

void loadStyleSheet(const QString& path)
{
    QFile f(path);
    if (!f.exists())
    {
        printf("Unable to set stylesheet, file not found\n");
    }
    else
    {
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    loadStyleSheet(":qdarkstyle/style.qss");

    Window w;

    w.showMaximized();
    return a.exec();
}
