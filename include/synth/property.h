#ifndef PROPERTY_H
#define PROPERTY_H

#include <string>
#include <vector>
#include <unordered_map>

using std::string;

namespace synth
{
   
class component;

template <class ValueType>
struct component_property
{
    using read_func  = ValueType (*)(component* src, const ValueType&);
    using write_func = ValueType (*)(component* src, const ValueType&);

    string name;
    ValueType value;
    read_func on_read;
    write_func on_write;
    
    component_property() :
        value(),
        on_read(nullptr),
        on_write(nullptr)
    {}
};

class property_map
{
public:
    property_map(component* parent) :
        mParent(parent)
    {}

    void set(const string& key, const float value)
    {
        auto it = mFloatProperties.find(key);
        if (it != mFloatProperties.end())
        {
            component_property<float>& prop = it->second;
            if (prop.on_write != nullptr)
                prop.value = prop.on_write(mParent, value);
            else prop.value = value;
        }
    }
    
    void set(const string& key, const string& value)
    {
        auto it = mStringProperties.find(key);
        if (it != mStringProperties.end())
        {
            component_property<string>& prop = it->second;
            if (prop.on_write != nullptr)
                prop.value = prop.on_write(mParent, value);
            else prop.value = value;
        }
    }
    
    float get_float(const string& key) const
    {
        auto it = mFloatProperties.find(key);
        if (it != mFloatProperties.end())
        {
            const component_property<float>& prop = it->second;
            if (prop.on_read != nullptr)
                return prop.on_read(mParent, prop.value);
            else return prop.value;
        }
        else return 0.0f;
    }
    
    string get_string(const string& key) const
    {
        auto it = mStringProperties.find(key);
        if (it != mStringProperties.end())
        {
            const component_property<string>& prop = it->second;
            if (prop.on_read != nullptr)
                return prop.on_read(mParent, prop.value);
            else return prop.value;
        }
        else return string();
    }
    
    const std::vector<string>& float_names() const
    {
        return mFloatPropertyNames;
    }
    
    const std::vector<string>& string_names() const
    {
        return mStringPropertyNames;
    } 
    
    void add(const component_property<float>& property)
    {
        mFloatProperties[property.name] = property;
        mFloatPropertyNames.push_back(property.name);
    }
    
    void add(const component_property<string>& property)
    {
        mStringProperties[property.name] = property;
        mStringPropertyNames.push_back(property.name);
    }

private:
    component* mParent;
    std::vector<string> mFloatPropertyNames;
    std::vector<string> mStringPropertyNames;
    std::unordered_map<string, component_property<float>> mFloatProperties;
    std::unordered_map<string, component_property<string>> mStringProperties;
};

}
#endif