#ifndef SYNTHESIZER_H
#define SYNTHESIZER_H

#include <iostream>
#include "synth/component.h"
#include "synth/components/variable.h"

namespace synth
{

/*class synthesizer
{
public:
        
    synthesizer() :
        mKeyFreq(0.0f),
        mOutput(nullptr)
    {}
    
    void generate(float* buffer, const uint32_t samples)
    {
        // Evaluate the graph backwards to generate each sample
        if (mOutput != nullptr)
        {
            component& out = *mOutput;
            for (uint32_t i = 0; i < samples; ++i)
                buffer[i] = out();
        }
    }
    
    void key_down(const float hz)
    {
        // Update the key frequency for any component that relies on it,
        // then pass the message that the key was pressed to each component
        // that relies on it.
        mKeyFreq(hz);
        mOutput->key_down();
    }
    
    void key_up()
    {
        // Pass the message that the key has been released to each component
        // in the graph.
        mOutput->key_up();
    }
    
    void attach_component_graph(component* graph)
    {
        mOutput = graph;
    }
    
    component* key_frequency()
    {
        return &mKeyFreq;
    }
            
private:
    variable mKeyFreq;
    component* mOutput;
};*/

}

#endif
