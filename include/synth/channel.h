#ifndef CHANNEL_H
#define CHANNEL_H

#include <vector>

namespace synth
{

class channel
{
public:

    channel(uint32_t samples_per_second) :
        mSamples(samples_per_second),
        mSamplesPerSecond(samples_per_second)
    {}
    
    void operator<<(const float x)
    {
        mSamples.push_back(x);
    }
    
    uint32_t samples_per_second() const
    {
        return mSamplesPerSecond;
    }
    
    const std::vector<float>& samples() const
    {
        return mSamples;
    }
    
private:
    std::vector<float> mSamples;
    uint32_t mSamplesPerSecond;
};

}

#endif