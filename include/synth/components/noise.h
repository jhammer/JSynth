#ifndef NOISE_H
#define NOISE_H

#include <random>
#include "synth/component.h"

namespace synth
{

struct noise : public component
{
    noise() :
        component(0),
        mDistribution(-1.0, 1.0)
    {}
    
    float operator()(int)
    {
        return mDistribution(mGenerator);
    }
    
private:
    std::default_random_engine mGenerator;
    std::uniform_real_distribution<float> mDistribution;
};

}
#endif
