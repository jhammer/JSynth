#ifndef PASS_THROUGH_H
#define PASS_THROUGH_H

#include "synth/component.h"
namespace synth
{

struct pass_through : public component
{
    pass_through() : component(1) {}

    float operator()(int) override
    {
        return get_predecessor(0)();
    }
};

}
#endif // PASS_THROUGH_H
