#ifndef VARIABLE_H
#define VARIABLE_H

#include "synth/component.h"
namespace synth
{

// A simple component that always returns the same value
struct variable : public component
{
    variable(float value = 0.0f) :
        component(0)
    {
        expose_property("value", value);
    }
    
    float operator()(int) const
    {
        return get_float_property("value");
    }
    float operator()(int) override
    {
        return get_float_property("value");
    }
    
    void operator()(float val)
    {
        set_property("value", val);
    }
};

}
#endif
