#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "synth/component.h"

namespace
{
    float note_to_freq(const int note_number, const int octave_offset)
    {
        // C0, C#0, D0, etc.
        static double base_frequencies[] =
            {16.35, 17.32, 18.35, 19.45, 20.60, 21.83, 23.12, 24.50, 25.96, 27.50, 29.14, 30.87};

        int pitch  = note_number % 12;
        int octave = (note_number / 12) - 1 + octave_offset;

        if (octave >= 0)
            return base_frequencies[pitch] * (1 << octave);
        else return base_frequencies[pitch] * pow(2.0, octave);
    }
}
namespace synth
{


class keyboard : public component
{
public:
    keyboard():
        component(0),
        mOctaveOffset(0)
    {}

    float operator()(int out) override
    {
        switch(out)
        {
            case 0:  return mFreq;
            case 1:  return mKeyDown ? 1.0f : 0.0f;
            default: return 0.0f;
        }
    }

    void key_down(int note_number)
    {
        mKeyDown = true;
        mFreq    = note_to_freq(note_number, mOctaveOffset);
    }

    void key_up(int /*note_number*/)
    {
        mKeyDown = false;
    }

    void set_octave_offset(int offset)
    {
        mOctaveOffset = offset;
    }

private:
    bool mKeyDown;
    float mFreq;
    int mOctaveOffset;
};

}
#endif // KEYBOARD_H
