#ifndef CROSS_FADE_H
#define CROSS_FADE_H

#include "synth/component.h"
namespace synth
{
    
struct cross_fade : public component
{
    cross_fade() :
        component(2),
        mMix(0.5f)
    {
        
    }
    
    float operator()(int)
    {
        float src1 = get_predecessor(0)();
        float src2 = get_predecessor(1)();
        
        return src1 * (1.0f - mMix) + src2 * mMix;
    }

    float mix() const
    {
        return mMix;
    }

    void mix(const float mix)
    {
        mMix = mix;
    }
    
private:
    float mMix;
};
}
#endif
