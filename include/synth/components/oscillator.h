#ifndef OSCILLATOR_H
#define OSCILLATOR_H

#include <string>
#include "synth/component.h"

using std::string;

namespace synth
{

// Property prototypes
inline string get_waveform(component* comp, const string& type);
inline string set_waveform(component* comp, const string& type);

class oscillator : public component
{
public:
    enum waveform_type {SIN, SQUARE, TRIANGLE, SAW};
    
    oscillator(uint32_t sample_rate) :
        component(2),
        mSampleRate(sample_rate),
        mCounter(0),
        mWaveformType(SIN),
        mOctaveOffset(0),
        mSemitoneOffset(0),
        mFineOffset(0)
    {
        expose_property("waveform", "sin", get_waveform, set_waveform);
    }
    
    float operator()(int) override
    {
        float freq = pitchBend(get_predecessor(0)());
        float amp  = get_predecessor(1)();
        // TODO: phase
        
        float mid = freq * mCounter / mSampleRate;
        float ret = 0.0f;
        switch (mWaveformType)
        {
            case SIN:
            {
                ret = sin(2.0f * M_PI * mid /*+ phase*/);
                break;
            }
            case SQUARE:
            {
                ret = 2.0f * (2.0f * floor(mid) - floor(2.0f * mid)) + 1.0f;
                break;
            }
            case TRIANGLE:
            {
                ret = 2.0f * fabs(2.0f * (mid - int(mid)) - 1.0f) - 1.0f;
                break;
            }
            case SAW:
            {
                ret = 2.0f * (mid - int(mid)) - 1.0f;
                break;
            }
             
            default: break;
        }

        mCounter++;
        return amp * ret;
        
    }
    
    // Getters / Setters
    waveform_type waveform() const
    {
        return mWaveformType;
    }
    
    void waveform(const waveform_type type)
    {
        mWaveformType = type;
    }
    
    void setOctaveOffset(int32_t octaveOffset)
    {
        mOctaveOffset = octaveOffset;
    }

    int32_t octaveOffset() const
    {
        return mOctaveOffset;
    }

    void setSemitoneOffset(int32_t semitoneOffset)
    {
        mSemitoneOffset = semitoneOffset;
    }

    int32_t semitoneOffset() const
    {
        return mSemitoneOffset;
    }

    void setFineOffset(int32_t fineOffset)
    {
        mFineOffset = fineOffset;
    }

    int32_t fineOffset() const
    {
        return mFineOffset;
    }

    void sampleRate(uint32_t sampleRate)
    {
        mSampleRate = sampleRate;
    }

    uint32_t sampleRate() const
    {
        return mSampleRate;
    }

private:
    uint32_t mSampleRate;
    uint32_t mCounter;
    waveform_type mWaveformType;

    // Pitch bend
    int32_t mOctaveOffset;
    int32_t mSemitoneOffset;
    int32_t mFineOffset;

    float pitchBend(float freq)
    {
        if (mOctaveOffset != 0 || mSemitoneOffset != 0 || mFineOffset != 0)
        {
            int32_t cents = mOctaveOffset * 1200 + mSemitoneOffset * 100 + mFineOffset;
            freq *= pow(2.0, cents/1200.0);
        }
        return freq;
    }
};

string get_waveform(component* comp, const string&)
{
    oscillator* me = (oscillator*) comp;
    oscillator::waveform_type type = me->waveform();
    
    switch(type)
    {
        case oscillator::waveform_type::SIN:      return "sin";
        case oscillator::waveform_type::SQUARE:   return "square";
        case oscillator::waveform_type::TRIANGLE: return "triangle";
        case oscillator::waveform_type::SAW:      return "saw";
        default:                                  return "[UNKNOWN]";
    }
}

string set_waveform(component* comp, const string& type)
{
    oscillator* me = (oscillator*) comp;
    if (type == "sin")
        me->waveform(oscillator::waveform_type::SIN);
    else if (type == "square")
        me->waveform(oscillator::waveform_type::SQUARE);
    else if (type == "triangle")
        me->waveform(oscillator::waveform_type::TRIANGLE);
    else if (type == "saw")
        me->waveform(oscillator::waveform_type::SAW);
    return type;
}
 
}
#endif
