#ifndef MULTIPLIER_H
#define MULTIPLIER_H

#include "synth/component.h"
namespace synth
{

// Multiplies the values of two signals together. Used can be used
// to apply envelopes, for example.
struct multiplier : public component
{
    multiplier() :
        component(2)
    {}
    
    float operator()(int) override
    {
        float signal   = get_predecessor(0)();
        float envelope = get_predecessor(1)();
        return signal * envelope;
    }
};

}
#endif
