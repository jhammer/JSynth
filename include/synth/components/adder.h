#ifndef ADDER_H
#define ADDER_H

#include "synth/component.h"
namespace synth
{

// Adds the values of two signals together. 
// NOTE: No normalization is performed, so a multiplier component
// should be added in series if that behavior is desired.
struct adder : public component
{
    adder() :
        component(2)
    {}
    
    float operator()(int) override
    {
        float c1 = get_predecessor(0)();
        float c2 = get_predecessor(1)();
        return c1 + c2;
    }
};

}
#endif
