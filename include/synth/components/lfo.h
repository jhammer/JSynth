#ifndef LFO_H
#define LFO_H

#include "synth/component.h"
#include "synth/components/oscillator.h"
#include "synth/components/variable.h"

namespace synth
{

class lfo : public component
{
public:
    lfo(uint32_t sampleRate) :
        component(0),
        mOscillator(sampleRate),
        mFrequencyVariable(20.0f),
        mAmplitudeVariable(1.0f),
        mMinValue(-1.0f),
        mMaxValue(1.0f)
    {
        mOscillator.set_predecessor(0, &mFrequencyVariable, 0);
        mOscillator.set_predecessor(1, &mAmplitudeVariable, 0);
    }

    float operator()(int)
    {
        // Set frequency

        // Map from [-1, 1] to [min, max]
        float ret = ((mOscillator(0) + 1.0f) * 0.5f) * (mMaxValue - mMinValue) + mMinValue;
        //printf("%f\n", ret);
        return ret;
    }

    oscillator::waveform_type waveform() const
    {
        return mOscillator.waveform();
    }
    void waveform(const oscillator::waveform_type type)
    {
        mOscillator.waveform(type);
    }
    void frequency(const float frequency)
    {
        mFrequencyVariable(frequency);
    }
    float frequency() const
    {
        return mFrequencyVariable(0);
    }
    void minValue(const float minValue)
    {
        mMinValue = minValue;
    }
    float minValue() const
    {
        return mMinValue;
    }
    void maxValue(const float maxValue)
    {
        mMaxValue = maxValue;
    }
    float maxValue() const
    {
        return mMaxValue;
    }
    void sampleRate(uint32_t sampleRate)
    {
        mOscillator.sampleRate(sampleRate);
    }
    uint32_t sampleRate() const
    {
        return mOscillator.sampleRate();
    }

private:
    oscillator mOscillator;
    variable mFrequencyVariable;
    variable mAmplitudeVariable;

    float mMinValue;
    float mMaxValue;
};

}
#endif // LFO_H
