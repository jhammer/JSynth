#ifndef LOW_PASS_H
#define LOW_PASS_H

#include "synth/component.h"
#include <cmath>

namespace synth
{

class low_pass : public component
{
public:
    enum filter_type {SIMPLE, BUTTERWORTH};
    
    low_pass(uint32_t sample_rate) :
        component(2),
        mSampleRate(sample_rate),
        mFilterType(SIMPLE),
        mCutoffFreq(25000.0f),
        mLastX(0.0f),
        mLastX2(0.0f),
        mLastY(0.0f),
        mLastY2(0.0f)
    {}
    
    float operator()(int)
    {
        float signal = get_predecessor(0)();
        float freq   = get_predecessor(1)();
        float res    = 0.0f;
        
        // Use the cutoff frequency when there is no imput
        if (get_predecessor(1).comp == nullptr)
            freq = mCutoffFreq;

        // Slight optimization - Don't work when there is no signal
        if (signal != 0.0f)
        {
            switch(mFilterType)
            {
                // https://www.embeddedrelated.com/showarticle/779.php
                case SIMPLE:
                {
                    // res = lastY + alpha * (signal - lastY)
                    // res = lastY + alpha * signal - alpha * lastY
                    // res = alpha * signal + lastY - alpha * lastY
                    // res = alpha * signal + (1 - alpha) * lastY
                    float alpha = freq / mSampleRate;
                    res         = mLastY + alpha * (signal - mLastY);
                    break;
                }

                // https://dsp.stackexchange.com/questions/8693/how-does-a-low-pass-filter-programmatically-work
                case BUTTERWORTH:
                {
                    // Calculate the Butterworth coefficients
                    float alpha        = std::tan(M_PI * freq / mSampleRate);
                    float root2Alpha   = std::sqrt(2.0f) * alpha;
                    float alphaSquared = alpha * alpha;
                    float denom        = 1.0f + root2Alpha + alphaSquared;

                    float k  = alphaSquared / denom;
                    float a1 = (2.0f * (alphaSquared - 1.0f)) / denom;
                    float a2 = (1.0f - root2Alpha + alphaSquared) / denom;
                    float b1 = 2.0f;
                    float b2 = 1.0f;

                    // Perform the filtering based on the coefficients
                    res = k * signal + k * b1 * mLastX + k * b2 * mLastX2
                        - a1 * mLastY - a2 * mLastY2;
                    res = std::min(std::max(res, -1.0f), 1.0f);
                    break;
                }
            }
        }
        
        // Update the history
        mLastX2 = mLastX;
        mLastY2 = mLastY;
        mLastX  = signal;
        mLastY  = res;
        return res;
    }
    
    filter_type filter() const
    {
        return mFilterType;
    }
    
    void filter(const filter_type type)
    {
        mFilterType = type;
    }

    float cutoff_frequency() const
    {
        return mCutoffFreq;
    }

    void cutoff_frequency(const float freq)
    {
        mCutoffFreq = freq;
    }

    void sampleRate(uint32_t sampleRate)
    {
        mSampleRate = sampleRate;
    }

    uint32_t sampleRate() const
    {
        return mSampleRate;
    }
    
private:
    uint32_t mSampleRate;
    filter_type mFilterType;
    float mCutoffFreq;
    
    // History
    float mLastX;
    float mLastX2;
    float mLastY;
    float mLastY2;
};

}
#endif
