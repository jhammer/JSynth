#ifndef ENVELOPE_H
#define ENVELOPE_H

#include "synth/component.h"

namespace synth
{

// Forward declarations of property functions
inline float get_attack_seconds(component* parent, const float& secs);
inline float set_attack_seconds(component* parent, const float& secs);
inline float get_decay_seconds(component* parent, const float& secs);
inline float set_decay_seconds(component* parent, const float& secs);
inline float get_release_seconds(component* parent, const float& secs);
inline float set_release_seconds(component* parent, const float& secs);
inline float set_sustain_amplitude(component* parent, const float& amp);

class adsr_envelope : public component
{
public:
    enum State {DISABLED, ATTACK, DECAY, SUSTAIN, RELEASE};
    
    adsr_envelope(uint32_t sample_rate):
        component(1),
        mSampleRate(sample_rate),
        mState(DISABLED),
        mAttackRate(0.0f),
        mDecayRate(0.0f),
        mReleaseRate(0.0f),
        mLastControl(0.0f),
        mLastAmplitude(0.0f)
    {
        expose_property("attack seconds",    0.0f, get_attack_seconds,  set_attack_seconds);
        expose_property("decay seconds",     0.0f, get_decay_seconds,   set_decay_seconds);
        expose_property("release seconds",   0.0f, get_release_seconds, set_release_seconds);
        expose_property("sustain amplitude", 1.0f, nullptr,             set_sustain_amplitude);
    }
    
    void on_key_down()
    {
        if (mAttackRate > 0.0f)
        {
            mState         = ATTACK;
            mLastAmplitude = 0.0f;
        }
        else
        {
            mState         = SUSTAIN;
            mLastAmplitude = sustainAmplitude();
        }
    }
    
    void on_key_up()
    {
        if (mReleaseRate > 0.0f)
        {
            mState = RELEASE;
        }
        else
        {
            mState = DISABLED;
        }
    }
    
    float operator()(int) override
    {
        float control = get_predecessor(0)();

        // Key down
        if (mLastControl <= 0.0f && control > 0.0f)
            on_key_down();

        // Key up
        else if (mLastControl > 0.0f && control <= 0.0f)
            on_key_up();

        float ret = mLastAmplitude;
        switch (mState)
        {
            case DISABLED:
            {
                ret = 0.0f;
                break;
            }
            case ATTACK:
            {
                ret += mAttackRate;
                if (ret >= 1.0f)
                {
                    mState = DECAY;
                    ret    = 1.0f;
                }
                break;
            }
            case DECAY:
            {
                float sa = sustainAmplitude();
                ret     -= mDecayRate;
                if (ret < sa)
                {
                    mState = SUSTAIN;
                    ret    = sa;
                }
                break;
            }
            case RELEASE:
            {
                ret -= mReleaseRate;
                if (ret < 0.0f)
                {
                    mState = DISABLED;
                    ret    = 0.0f;
                }
                break;
            }
            default: break;
        }
        
        mLastControl   = control;
        mLastAmplitude = ret;
        return ret;
    }
    
    // Setters
    void sampleRate(const uint32_t sample_rate)
    {
        float attack_seconds  = get_attack_seconds(this, 0.0f);
        float decay_seconds   = get_decay_seconds(this, 0.0f);
        float release_seconds = get_release_seconds(this, 0.0f);
        mSampleRate = sample_rate;
        set_attack_seconds(this, attack_seconds);
        set_decay_seconds(this, decay_seconds);
        set_release_seconds(this, release_seconds);
    }

    void attackRate(const float rate)  { mAttackRate  = rate; }
    void decayRate(const float rate)   { mDecayRate   = rate; }
    void releaseRate(const float rate) { mReleaseRate = rate; }
   
    // Getters
    uint32_t sampleRate() const    { return mSampleRate;                             }
    float attackRate()    const    { return mAttackRate;                             }
    float decayRate()     const    { return mDecayRate;                              }
    float releaseRate()   const    { return mReleaseRate;                            }
    float sustainAmplitude() const { return get_float_property("sustain amplitude"); }

private: 

    uint32_t mSampleRate;
    State mState;
    
    float mAttackRate;
    float mDecayRate;
    float mReleaseRate;
    
    float mLastControl;
    float mLastAmplitude;
};

// Property functions - We show the user a nicer interface than is used internally.

float get_attack_seconds(component* parent, const float&)
{
    adsr_envelope* me   = (adsr_envelope*) parent;
    float attackRate    = me->attackRate();
    uint32_t sampleRate = me->sampleRate();

    // rate = 1 / (sampleRate * secs - 1)
    // (sampleRate * secs - 1) * rate = 1
    // sampleRate * secs - 1 = 1/rate
    // sampleRate * secs = (1/rate) + 1
    // secs = ((1/rate) + 1) / sampleRate
    // secs = (1/rate + rate/rate) / sampleRate
    // secs = ((1 + rate) / rate) * (1 / sampleRate)
    // secs = (1 + rate) / (rate * sampleRate)

    if (attackRate <= 0.0f)
        return 0.0f;
    else return (1.0f + attackRate) / (attackRate * sampleRate);
}

float set_attack_seconds(component* parent, const float& secs)
{
    adsr_envelope* me   = (adsr_envelope*) parent;
    uint32_t sampleRate = me->sampleRate();

    if (secs <= 0.0f)
        me->attackRate(0.0f);
    else me->attackRate(1.0f / (sampleRate * secs - 1));
    return secs;
}

float get_decay_seconds(component* parent, const float&)
{
    adsr_envelope* me      = (adsr_envelope*) parent;
    float decayRate        = me->decayRate();
    float sustainAmplitude = me->sustainAmplitude();
    uint32_t sampleRate    = me->sampleRate();


    // mDecayRate = (1.0f - mSustainAmplitude) / (mSampleRate * secs - 1)
    // mDecayRate * (mSampleRate * secs - 1) = 1 - mSustainAmplitude
    // mSampleRate * secs - 1 = (1 - mSustainAmplitude) / mDecayRate
    // mSampleRate * secs = ((1 - mSustainAmplitude) / mDecayRate) + 1
    // secs = (((1 - mSustainAmplitude) / mDecayRate) + 1) / mSampleRate

    if (decayRate <= 0.0f)
        return 0.0f;
    else return (((1.0f - sustainAmplitude) / decayRate) + 1.0f) / sampleRate;
}

float set_decay_seconds(component* parent, const float& secs)
{
    adsr_envelope* me      = (adsr_envelope*) parent;
    uint32_t sampleRate    = me->sampleRate();
    float sustainAmplitude = me->sustainAmplitude();

    if (secs <= 0.0f)
        me->decayRate(0.0f);
    else me->decayRate((1.0f - sustainAmplitude) / (sampleRate * secs - 1.0f));
    return secs;
}

float get_release_seconds(component* parent, const float&)
{
    adsr_envelope* me      = (adsr_envelope*) parent;
    float releaseRate      = me->releaseRate();
    float sustainAmplitude = me->sustainAmplitude();
    uint32_t sampleRate    = me->sampleRate();

    // mReleaseRate = mSustainAmplitude / (mSampleRate * secs - 1)
    // mReleaseRate * (mSampleRate * secs - 1) = mSustainAmplitude
    // mSampleRate * secs - 1 = mSustainAmplitude / mReleaseRate
    // mSampleRate * secs = (mSustainAmplitude / mReleaseRate) + 1
    // secs = ((mSustainAmplitude / mReleaseRate) + 1) / mSampleRate

    if (releaseRate <= 0.0f)
        return 0.0f;
    else return ((sustainAmplitude / releaseRate) + 1.0f) / sampleRate;
}

float set_release_seconds(component* parent, const float& secs)
{
    adsr_envelope* me      = (adsr_envelope*) parent;
    float sustainAmplitude = me->sustainAmplitude();
    uint32_t sampleRate    = me->sampleRate();

    if (secs <= 0.0f)
        me->releaseRate(0.0f);
    else me->releaseRate(sustainAmplitude / (sampleRate * secs - 1));
    return secs;
}

float set_sustain_amplitude(component*, const float& amp)
{
    if (amp < 0.0f)
        return 0.0f;
    else if (amp > 1.0f)
        return 1.0f;
    else return amp;
}

}
#endif
