#ifndef COMPONENT_H
#define COMPONENT_H

#include <vector>
#include <string>
#include "synth/property.h"

using std::string;

namespace synth
{

class component
{
public:

    // An endpoint contains both a component and a port number.
    struct endpoint
    {
        component* comp;
        int port;

        // Constructors
        endpoint() : comp(nullptr), port(0) {}
        endpoint(component* comp, int port) : comp(comp), port(port) {}

        float operator()()
        {
            if (comp != nullptr)
                return (*comp)(port);
            else return 0.0f;
        }
    };

    // Constructors
    component(size_t num_predecessors) :
        mPredecessors(num_predecessors),
        mProperties(this)
    {}
    
    virtual ~component() {}
    
    // Required / optional interfaces
    virtual float operator()(int port) = 0;

    // Predecessors
    void set_predecessor(const size_t index, component* predecessor, int port)
    {
        mPredecessors[index] = endpoint(predecessor, port);
    }
    
    endpoint get_predecessor(const size_t index)
    {
        return mPredecessors[index];
    }
    
    // Property handling
    void set_property(const string& key, const float value)
    {
        mProperties.set(key, value);
    }
    
    void set_property(const string& key, const string& value)
    {
        mProperties.set(key, value);
    }
    
    float get_float_property(const string& key) const
    {
        return mProperties.get_float(key);
    }
    
    string get_string_property(const string& key) const
    {
        return mProperties.get_string(key);
    }
    
    const std::vector<string>& float_property_names() const
    {
        return mProperties.float_names();
    }
    
    const std::vector<string>& string_property_names() const
    {
        return mProperties.string_names();
    }
    
protected:

    void expose_property(const string& name, const float initial_value, 
        component_property<float>::read_func on_read   = nullptr, 
        component_property<float>::write_func on_write = nullptr)
    {
        component_property<float> prop;
        prop.name     = name;
        prop.value    = initial_value;
        prop.on_read  = on_read;
        prop.on_write = on_write;
        
        mProperties.add(prop);
    }
    
    void expose_property(const string& name, const string& initial_value, 
        component_property<string>::read_func on_read   = nullptr, 
        component_property<string>::write_func on_write = nullptr)
    {
        component_property<string> prop;
        prop.name     = name;
        prop.value    = initial_value;
        prop.on_read  = on_read;
        prop.on_write = on_write;
        
        mProperties.add(prop);
    }
    
private:
    std::vector<endpoint> mPredecessors;
    property_map mProperties;
};

}

#endif
