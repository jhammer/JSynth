#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include "component_frame.h"
#include "widgets/waveform.h"

namespace Ui {
class Window;
}

class ComponentFrame;

class Window : public QMainWindow
{
    Q_OBJECT

public:
    explicit Window(QWidget *parent = 0);
    ~Window();

signals:
    void onSampleRateChanged(int sampleRate);
    void onCableTensionChanged(double tension);
    void onCableOpacityChanged(double opacity);

private slots:
    void addSynthComponent(const QString& text);

private:
    Ui::Window *ui;
    uint32_t mSampleRate;

    // Canvas components
    ComponentFrame* mComponentFrame;
};

#endif // WINDOW_H
