#ifndef LFO_WIDGET_H
#define LFO_WIDGET_H

#include "widgets/component_widget.h"
#include "widgets/simple_dial.h"
#include "synth/components/lfo.h"

class LFOWidget : public ComponentWidget
{
public:
    LFOWidget(uint32_t sampleRate, QWidget* parent = nullptr) :
        ComponentWidget(new synth::lfo(sampleRate), parent)
    {
        setName("LFO");
        setOutputNames({"Out"});
        setOutputColors({ComponentWidget::COLOR_CONTROL});

        SimpleDial* waveform  = new SimpleDial(0, 3, "Waveform");
        SimpleDial* frequency = new SimpleDial(0, 100, "Frequency");
        SimpleDial* minValue  = new SimpleDial(-20000, 20000, "Min");
        SimpleDial* maxValue  = new SimpleDial(-20000, 20000, "Max");

        frequency->setValue(20);
        frequency->setDefaultValue(20);
        minValue->setDefaultValue(-1);
        maxValue->setDefaultValue(1);

        QVBoxLayout* layout = new QVBoxLayout();
        layout->setSpacing(0);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->addWidget(waveform, 1);

        QHBoxLayout* controls = new QHBoxLayout();
        controls->setSpacing(0);
        controls->setContentsMargins(0, 0, 0, 0);
        controls->addWidget(frequency);
        controls->addWidget(minValue);
        controls->addWidget(maxValue);
        layout->addLayout(controls, 1);

        QWidget* contentPane = new QWidget();
        contentPane->setLayout(layout);
        setContentPane(contentPane);

        connect(waveform, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
        {
            synth::lfo* comp = (synth::lfo*) getComponent();
            switch (val)
            {
                case 0: comp->waveform(synth::oscillator::waveform_type::SIN);      break;
                case 1: comp->waveform(synth::oscillator::waveform_type::TRIANGLE); break;
                case 2: comp->waveform(synth::oscillator::waveform_type::SAW);      break;
                case 3: comp->waveform(synth::oscillator::waveform_type::SQUARE);   break;
            }
        });

        connect(frequency, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
        {
            synth::lfo* comp = (synth::lfo*) getComponent();
            comp->frequency((float) val);
        });
        connect(minValue, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
        {
            synth::lfo* comp = (synth::lfo*) getComponent();
            comp->minValue((float) val);
        });
        connect(maxValue, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
        {
            synth::lfo* comp = (synth::lfo*) getComponent();
            comp->maxValue((float) val);
        });

        Window* window = static_cast<Window*>(QApplication::activeWindow());
        connect(window, &Window::onSampleRateChanged, [=](int sampleRate)
        {
            ((synth::lfo*) getComponent())->sampleRate(sampleRate);
        });
    }
};
#endif // LFO_WIDGET_H
