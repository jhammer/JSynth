#ifndef MIDI_WIDGET_H
#define MIDI_WIDGET_H

#include <QWidget>
#include <QComboBox>
#include "widgets/component_widget.h"
#include "RtMidi.h"

class MidiWidget : public ComponentWidget
{
    Q_OBJECT

public:
    MidiWidget(QWidget* parent = nullptr);
    ~MidiWidget();

    void noteOn(int note, int pressure, int channel);
    void noteOff(int note, int channel);
    
private:
    RtMidiIn* mInput;
    QComboBox* mMidiDevices;
};
#endif
