#ifndef OSCILLATORWIDGET_H
#define OSCILLATORWIDGET_H

#include "widgets/component_widget.h"

class OscillatorWidget : public ComponentWidget
{
public:
    OscillatorWidget(uint32_t sampleRate, QWidget* parent = nullptr);
};

#endif // OSCILLATORWIDGET_H
