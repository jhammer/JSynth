#ifndef SIMPLE_DIAL_H
#define SIMPLE_DIAL_H

#include <QWidget>
#include <QDial>
#include <QLabel>
#include <QMouseEvent>

class SimpleDial : public QWidget
{
    Q_OBJECT

public:
    SimpleDial(int minValue, int maxValue, const QString& text, QWidget* parent = nullptr);

signals:
    void onValueChanged(int value);

public slots:
    void setValue(int value);
    void setDefaultValue(int value);

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void contextMenuEvent(QContextMenuEvent *event) override;

private:
    QDial* mDial;
    QLabel* mLabel;
    int mDefaultValue;
};

#endif // SIMPLE_DIAL_H
