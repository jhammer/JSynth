#ifndef KEYBOARD_WIDGET_H
#define KEYBOARD_WIDGET_H

#include <QWidget>
#include <QKeyEvent>
#include "widgets/component_widget.h"

class KeyboardWidget : public ComponentWidget
{
    Q_OBJECT

public:
    KeyboardWidget(QWidget* parent = nullptr);

public slots:

protected:
    void keyPressEvent(QKeyEvent* event) override;
    void keyReleaseEvent(QKeyEvent* event) override;

private:

};

#endif // KEYBOARD_WIDGET_H
