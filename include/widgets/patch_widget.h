#ifndef PATCH_WIDGET_H
#define PATCH_WIDGET_H

#include <QWidget>
#include "component_widget.h"

class PatchWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PatchWidget(QWidget *parent = nullptr);
    ~PatchWidget();

    void setEndpoint(QPoint endpoint);
    void removeEndpoint();

    // Setters
    void setSource(ComponentWidget* source, int sourcePort);
    void setDest(ComponentWidget* dest, int destPort);
    void setColor(QColor color);
    void setConnectionTension(double tension);

    // Getters
    ComponentWidget* source() const;
    ComponentWidget* dest() const;
    int sourcePort() const;
    int destPort() const;
    QColor color() const;
    double connectionTension() const;

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    ComponentWidget* mSource;
    ComponentWidget* mDest;
    int mSourcePort;
    int mDestPort;

    // For drawing temporary patches
    QPoint* mEndpoint;

    QColor mColor;
    double mConnectionTension;
};

#endif // PATCH_WIDGET_H
