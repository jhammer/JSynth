#ifndef CROSS_FADE_WIDGET_H
#define CROSS_FADE_WIDGET_H

#include "widgets/component_widget.h"
#include "synth/components/cross_fade.h"

class CrossFadeWidget : public ComponentWidget
{
public:
    CrossFadeWidget(QWidget* parent = nullptr) :
        ComponentWidget(new synth::cross_fade(), parent)
    {
        setName("Cross Fade");
        setInputNames({"Ch1", "Ch2"});
        setOutputNames({"Out"});
        setInputColors({ComponentWidget::COLOR_SOUND, ComponentWidget::COLOR_SOUND});
        setOutputColors({ComponentWidget::COLOR_SOUND});

        SimpleDial* alpha = new SimpleDial(0, 100, "Mix");
        alpha->setDefaultValue(50);
        alpha->setValue(50);

        QGridLayout* layout = new QGridLayout();
        layout->addWidget(alpha);

        QWidget* contentPane = new QWidget();
        contentPane->setLayout(layout);
        setContentPane(contentPane);

        connect(alpha, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
        {
            synth::cross_fade* comp = (synth::cross_fade*) getComponent();
            comp->mix(val / 100.0f);
        });
    }
};
#endif // CROSS_FADE_WIDGET_H
