#ifndef SPEAKER_WIDGET_H
#define SPEAKER_WIDGET_H

#include <cmath>
#include <QWidget>
#include <QComboBox>
#include <QIODevice>
#include <QAudioFormat>
#include <QtMultimedia/QAudioOutput>
#include "widgets/component_widget.h"

class SpeakerWidget : public ComponentWidget
{
    Q_OBJECT

public:
    SpeakerWidget(uint32_t sampleRate, QWidget* parent = nullptr);
    virtual ~SpeakerWidget();

private slots:
    void generateData();

private:
    uint32_t mSampleRate;

    QIODevice* mGenerator;
    QAudioOutput* mSpeaker;

    QComboBox* mAudioDevicesBox;
    QComboBox* mBlockSizeBox;

    void initAudio(uint32_t sampleRate, uint32_t blockSize);
    void stopAudio();
    QAudioFormat getPreferredAudioFormat(uint32_t sampleRate) const;
};

#endif // SPEAKER_WIDGET_H
