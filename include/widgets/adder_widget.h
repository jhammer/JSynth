#ifndef ADDER_WIDGET_H
#define ADDER_WIDGET_H

#include "widgets/component_widget.h"
#include "synth/components/adder.h"

class AdderWidget : public ComponentWidget
{
public:
    AdderWidget(QWidget* parent = nullptr) :
        ComponentWidget(new synth::adder(), parent)
    {
        setName("Adder");
        setInputNames({"In1", "In2"});
        setOutputNames({"Out"});
        setInputColors({ComponentWidget::COLOR_ANY, ComponentWidget::COLOR_ANY});
        setOutputColors({ComponentWidget::COLOR_ANY});
    }
};

#endif // ADDER_WIDGET_H
