#ifndef INDICATOR_LABEL_H
#define INDICATOR_LABEL_H

#include <QWidget>
#include <QLabel>

class IndicatorLabel : public QWidget
{
    Q_OBJECT
public:
    static const int DIRECTION_LEFT  = 0;
    static const int DIRECTION_RIGHT = 1;

    explicit IndicatorLabel(int direction, QWidget *parent = nullptr);

    double brightness() const;
    QColor color() const;
    QString text() const;

signals:

public slots:
    void setBrightness(double value);
    void setColor(QColor color);
    void setText(QString text);
    void setAlignment(Qt::Alignment align);

private:
    class Indicator : public QWidget
    {
    public:
        explicit Indicator(QWidget* parent = nullptr);
        void setColor(QColor color);
        void setBrightness(double brightness);

        QColor color() const;
        double brightness() const;

    protected:
        void paintEvent(QPaintEvent *event) override;

    private:
        QColor mColor;
        double mBrightness;
    };

    QLabel* mLabel;
    Indicator* mIndicator;
};

#endif // INDICATOR_LABEL_H
