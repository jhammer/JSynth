#ifndef COMPONENT_WIDGET_H
#define COMPONENT_WIDGET_H

#include <QFrame>
#include <QWidget>
#include <QLabel>
#include <QString>
#include <QList>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QStringList>
#include <QDial>
#include <vector>
#include "widgets/indicator_label.h"
#include "synth/component.h"

class ComponentWidget : public QFrame
{
    Q_OBJECT
public:
    explicit ComponentWidget(synth::component* component, QWidget *parent = nullptr);
    virtual ~ComponentWidget();

    void setName(const QString& name);
    void setInputNames(const QStringList& inputNames);
    void setOutputNames(const QStringList& outputNames);
    void setInputColors(const std::vector<QColor> inputColors);
    void setOutputColors(const std::vector<QColor> outputColors);
    void setContentPane(QWidget* contentPane);

    // Returns the rect describing the name label in the ComponentWidget coordinates.
    QRect getNameRect() const;

    // Returns the index of the input at the given point in ComponentWidget coordinates.
    // If no input is valid, -1 will be returned.
    int inputIndex(const QPoint& pos) const;

    // Returns the index of the output at the given point in ComponentWidget coordinates.
    // If no output is valid, -1 will be returned.
    int outputIndex(const QPoint& pos) const;

    // Returns the offset of the given input relative to the top left
    // corner of the ComponentWidget.
    QPoint inputOffset(const int index) const;

    // Returns the offset of the given output relative to the top left
    // corner of the ComponentWidget.
    QPoint outputOffset(const int index) const;

    // Returns the component owned by this widget
    synth::component* getComponent() const;

    QColor inputColor(const int index) const;
    QColor outputColor(const int index) const;

    static const QColor COLOR_SOUND;
    static const QColor COLOR_CONTROL;
    static const QColor COLOR_FREQ;
    static const QColor COLOR_ANY;

signals:

public slots:

protected:
    synth::component* mComponent;

private:
    QLabel* mNameLabel;
    QVBoxLayout* mInputsLayout;
    QVBoxLayout* mOutputsLayout;
    QGridLayout* mGrid;

    QList<IndicatorLabel*> mInputLabels;
    QList<IndicatorLabel*> mOutputLabels;
};

#endif // COMPONENT_WIDGET_H
