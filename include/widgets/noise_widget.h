#ifndef NOISE_WIDGET_H
#define NOISE_WIDGET_H

#include "widgets/component_widget.h"
#include "synth/components/noise.h"

class NoiseWidget : public ComponentWidget
{
public:
    NoiseWidget(QWidget* parent = nullptr) :
        ComponentWidget(new synth::noise(), parent)
    {
        setName("Noise");
        setOutputNames({"Out"});
        setOutputColors({ComponentWidget::COLOR_SOUND});

        QWidget* contentPane = new QWidget();
        setContentPane(contentPane);
    }
};
#endif // NOISE_WIDGET_H
