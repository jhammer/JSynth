#ifndef WAVEFORM_H
#define WAVEFORM_H

#include <QWidget>
#include <QPaintEvent>
#include <QResizeEvent>

class Waveform : public QWidget
{
    Q_OBJECT
public:
    explicit Waveform(size_t sampleRate, double seconds, size_t resolution, QWidget *parent = nullptr);
    ~Waveform();

    // Write the audio samples to the internal buffer so they
    // can be displayed.
    void write(float* samples, size_t count);

    // Write a single audio sample to the internal buffer so it can
    // be displayed.
    void write(float sample);

    // Adjust the interval at which samples are taken. Must be >= 1,
    // but larger values will be more efficient.
    void setResolution(size_t resolution);

    // Update the sample rate
    void setSampleRate(size_t sampleRate);

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    // The circular buffer. Older samples are replaced with
    // newer ones.
    QPointF* mBuffer;
    size_t mBufferSize;
    size_t mCurIndex;
    double mDurationSeconds;

    // The interval at which samples are taken.
    size_t mResolution;
    size_t mSingleCounter;

    // Controls window scrolling
    double mWindowStart;
    double mWindowEnd;
};

#endif // WAVEFORM_H
