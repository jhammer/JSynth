#ifndef LOW_PASS_WIDGET_H
#define LOW_PASS_WIDGET_H

#include "widgets/component_widget.h"
#include "widgets/simple_dial.h"
#include "synth/components/low_pass.h"

class LowPassWidget : public ComponentWidget
{
public:
    LowPassWidget(uint32_t sampleRate, QWidget* parent = nullptr) :
        ComponentWidget(new synth::low_pass(sampleRate), parent)
    {
        setName("Low Pass");
        setInputNames({"Signal", "Freq"});
        setOutputNames({"Out"});
        setInputColors({ComponentWidget::COLOR_SOUND, ComponentWidget::COLOR_FREQ});
        setOutputColors({ComponentWidget::COLOR_SOUND});

        SimpleDial* filterType = new SimpleDial(0, 1, "Type");
        SimpleDial* cutoffFreq = new SimpleDial(0, 25000, "Freq");
        cutoffFreq->setDefaultValue(25000);
        cutoffFreq->setValue(25000);

        QHBoxLayout* layout = new QHBoxLayout();
        layout->addWidget(filterType);
        layout->addWidget(cutoffFreq);

        QWidget* contentPane = new QWidget();
        contentPane->setLayout(layout);
        setContentPane(contentPane);

        connect(filterType, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
        {
            synth::low_pass* comp = (synth::low_pass*) getComponent();
            switch(val)
            {
                case 0: comp->filter(synth::low_pass::filter_type::SIMPLE);      break;
                case 1: comp->filter(synth::low_pass::filter_type::BUTTERWORTH); break;
            }
        });

        connect(cutoffFreq, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
        {
            synth::low_pass* comp = (synth::low_pass*) getComponent();
            comp->cutoff_frequency((float) val);
        });


        Window* window = static_cast<Window*>(QApplication::activeWindow());
        connect(window, &Window::onSampleRateChanged, [=](int sampleRate)
        {
            ((synth::low_pass*) getComponent())->sampleRate(sampleRate);
        });
    }
};
#endif // LOW_PASS_WIDGET_H
