#ifndef VISUALIZER_WIDGET_H
#define VISUALIZER_WIDGET_H

#include "component_widget.h"
#include "widgets/waveform.h"
#include "synth/component.h"

class VisualizerWidget : public ComponentWidget
{
public:

    VisualizerWidget(uint32_t sampleRate, QWidget* parent = nullptr);

private:
    struct visualizer_component : public synth::component
    {
        visualizer_component(VisualizerWidget* parent) : component(1), parent(parent) {}

        float operator()(int) override
        {
            float sample = get_predecessor(0)();
            if (parent->mWaveform != nullptr) parent->mWaveform->write(sample);
            return sample;
        }

    private:
        VisualizerWidget* parent;
    };

    Waveform* mWaveform;
};

#endif // VISUALIZER_WIDGET_H
