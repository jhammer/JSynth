#ifndef VARIABLE_WIDGET_H
#define VARIABLE_WIDGET_H

#include "synth/components/variable.h"
#include "widgets/component_widget.h"
#include <QDoubleSpinBox>
#include <QWidget>

class VariableWidget : public ComponentWidget
{
public:
    VariableWidget(double value, QWidget* parent = nullptr):
        ComponentWidget(new synth::variable, parent)
    {
        setName("Variable");
        setOutputNames({"Out"});
        setOutputColors({ComponentWidget::COLOR_ANY});

        QLabel* text = new QLabel("Value");

        QDoubleSpinBox* edit = new QDoubleSpinBox();
        edit->setValue(value);
        edit->setMaximum(100000.0);

        QHBoxLayout* layout = new QHBoxLayout();
        layout->addWidget(text);
        layout->addWidget(edit);

        QWidget* contentPane = new QWidget();
        contentPane->setLayout(layout);
        setContentPane(contentPane);

        // Update the component when the value of the editor changes
        connect(edit, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double val)
        {
            synth::variable& var = *((synth::variable*) mComponent);
            var((float) val);
        });
    }
};

#endif // VARIABLE_WIDGET_H
