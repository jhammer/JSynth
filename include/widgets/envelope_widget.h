#ifndef ENVELOPE_WIDGET_H
#define ENVELOPE_WIDGET_H

#include "widgets/component_widget.h"
#include "widgets/simple_dial.h"
#include "synth/components/envelope.h"
#include <cmath>

// Finds a monotonically increasing power curve that passes
// through 'halfLevel' at 0.5 and 'fullLevel' and 1.0. Returns
// curve(x), where 0.0 <= x <= 1.0.
// curve(0) will always be 0.0.
double powerCurve(double x, double halfLevel, double fullLevel)
{
    const double gamma = log(halfLevel / fullLevel) / log(0.5);
    return fullLevel * pow(x, gamma);
}

class EnvelopeWidget : public ComponentWidget
{
public:
    EnvelopeWidget(uint32_t sampleRate, QWidget* parent = nullptr) :
        ComponentWidget(new synth::adsr_envelope(sampleRate), parent)
    {
        setName("ADSR");
        setInputNames({"In"});
        setOutputNames({"Out"});
        setInputColors({ComponentWidget::COLOR_CONTROL});
        setOutputColors({ComponentWidget::COLOR_CONTROL});

        SimpleDial* attackTime   = new SimpleDial(0, 10000, "Attack");
        SimpleDial* decayTime    = new SimpleDial(0, 10000, "Decay");
        SimpleDial* sustainLevel = new SimpleDial(0, 100,   "Sustain");
        SimpleDial* releaseTime  = new SimpleDial(0, 10000, "Release");
        sustainLevel->setDefaultValue(100);
        sustainLevel->setValue(100);

        QHBoxLayout* layout = new QHBoxLayout();
        layout->addWidget(attackTime);
        layout->addWidget(decayTime);
        layout->addWidget(sustainLevel);
        layout->addWidget(releaseTime);

        QWidget* contentPane = new QWidget();
        contentPane->setLayout(layout);
        setContentPane(contentPane);

        connect(attackTime, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
        {
            synth::adsr_envelope* comp = (synth::adsr_envelope*) getComponent();
            comp->set_property("attack seconds", powerCurve(val/10000.0, 1.0, 10.0));
        });

        connect(decayTime, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
        {
            synth::adsr_envelope* comp = (synth::adsr_envelope*) getComponent();
            comp->set_property("decay seconds", powerCurve(val/10000.0, 1.0, 10.0));
        });

        connect(sustainLevel, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
        {
            synth::adsr_envelope* comp = (synth::adsr_envelope*) getComponent();
            comp->set_property("sustain amplitude", val / 100.0f);
        });

        connect(releaseTime, QOverload<int>::of(&SimpleDial::onValueChanged), [=](int val)
        {
            synth::adsr_envelope* comp = (synth::adsr_envelope*) getComponent();
            comp->set_property("release seconds", powerCurve(val/10000.0, 1.0, 10.0));
        });


        Window* window = static_cast<Window*>(QApplication::activeWindow());
        connect(window, &Window::onSampleRateChanged, [=](int sampleRate)
        {
            ((synth::adsr_envelope*) getComponent())->sampleRate(sampleRate);
        });
    }

};
#endif // ENVELOPE_WIDGET_H
