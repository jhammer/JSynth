#ifndef MULTIPLIER_WIDGET_H
#define MULTIPLIER_WIDGET_H

#include "widgets/component_widget.h"
#include "synth/components/multiplier.h"

class MultiplierWidget : public ComponentWidget
{
public:
    MultiplierWidget(QWidget* parent = nullptr) :
        ComponentWidget(new synth::multiplier(), parent)
    {
        setName("Multiplier");
        setInputNames({"In1", "In2"});
        setOutputNames({"Out"});
        setInputColors({ComponentWidget::COLOR_ANY, ComponentWidget::COLOR_ANY});
        setOutputColors({ComponentWidget::COLOR_ANY});
    }
};
#endif // MULTIPLIER_WIDGET_H
