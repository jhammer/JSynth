#ifndef COMPONENT_FRAME_H
#define COMPONENT_FRAME_H

#include <QWidget>
#include "widgets/component_widget.h"
#include "widgets/patch_widget.h"
#include "window.h"

class Window;

// The ComponentFrame is the basis of the GUI. It allows child widgets
// to be dragged around the frame and patch connections to be drawn
// between widgets (especially ComponentWidgets).
class ComponentFrame : public QWidget
{
    Q_OBJECT

public:
    explicit ComponentFrame(QWidget* parent = nullptr);

    // Create a new patch between the two given components
    void patch(ComponentWidget* source, int sourcePort,
               ComponentWidget* dest, int destPort,
               QColor color = Qt::black, double connectionTension = 100.0);

    // Remove the given patch, disconnecting the two affected components
    void removePatch(PatchWidget* patch);

    // Add a new child to the frame. This method will reparent the child
    // and show() it, so the caller doesn't have to.
    void addChild(QWidget* widget);

    // Removes the given component and any patches that rely on it
    void removeComponent(ComponentWidget* child);

    // Sets the window that holds this frame. Allows us to
    // listen to changes in the global properties
    void setWindow(Window* window);

signals:
    void onResize(int width, int height);

protected:
    void mousePressEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;

    void resizeEvent(QResizeEvent* event) override;
    void contextMenuEvent(QContextMenuEvent *event) override;

private:

    // Handles the events required to move widgets around the screen
    struct WidgetDragger
    {
        WidgetDragger(ComponentFrame* parent) :
            parent(parent), dragWidget(nullptr) {}

        // Event handlers
        void onMousePress(QMouseEvent *event);
        void onMouseMove(QMouseEvent* event);
        void onMouseRelease(QMouseEvent* event);

        // Reference to the parent object
        ComponentFrame* parent;

        // Widget currently being dragged (or nullptr)
        QWidget* dragWidget;

        // Mouse hotspot
        QPoint hotspot;
    };

    // Handles the events required to draw patch connections
    struct PatchConnector
    {
        PatchConnector(ComponentFrame* parent) : parent(parent) {}

        // Event handlers
        void onMousePress(QMouseEvent* event);
        void onMouseMove(QMouseEvent* event);
        void onMouseRelease(QMouseEvent* event);

        // Reference to the parent object
        ComponentFrame* parent;
    };

    WidgetDragger mWidgetDragger;
    PatchConnector mPatchConnector;
    PatchWidget* mTempPatch;

    // Private functions
    ComponentWidget* componentAt(QPoint pos);
    void repaintAll();

    // Returns true if this potential patch conflicts with any others already present
    bool conflictingPatch(ComponentWidget* source, int sourcePort,
                          ComponentWidget* dest, int destPort);
};

#endif // COMPONENT_FRAME_H
