# Git Instructions
    git clone
    git submodule init
    git submodule update

# Build Instructions

Release build:
    cd ./bin
    qmake ../JSynth.pro
    make

Debug builds:
    cd ./bin
    qmake CONFIG+=debug ../JSynth.pro
    make

Make sure the correct compiler is used.
