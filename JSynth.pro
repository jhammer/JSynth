#-------------------------------------------------
#
# Project created by QtCreator 2018-03-25T17:18:50
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = JSynth
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Tell the compiler where to look for header files
INCLUDEPATH += $$PWD/include
INCLUDEPATH += $$PWD/dep/rtmidi

SOURCES += \
    src/main.cpp \
    src/window.cpp \
    src/widgets/waveform.cpp \
    src/widgets/component_widget.cpp \
    src/component_frame.cpp \
    src/widgets/oscillator_widget.cpp \
    src/widgets/simple_dial.cpp \
    src/widgets/speaker_widget.cpp \
    src/widgets/keyboard_widget.cpp \
    src/widgets/visualizer_widget.cpp \
    src/widgets/midi_widget.cpp \
    src/widgets/patch_widget.cpp \
    src/widgets/indicator_label.cpp \
    dep/rtmidi/RtMidi.cpp

HEADERS += \
    include/window.h \
    include/synth/components/adder.h \
    include/synth/components/envelope.h \
    include/synth/components/low_pass.h \
    include/synth/components/multiplier.h \
    include/synth/components/noise.h \
    include/synth/components/oscillator.h \
    include/synth/components/variable.h \
    include/synth/channel.h \
    include/synth/component.h \
    include/synth/property.h \
    include/widgets/waveform.h \
    include/widgets/component_widget.h \
    include/component_frame.h \
    include/widgets/oscillator_widget.h \
    include/widgets/simple_dial.h \
    include/widgets/variable_widget.h \
    include/widgets/speaker_widget.h \
    include/widgets/keyboard_widget.h \
    include/synth/components/keyboard.h \
    include/synth/components/pass_through.h \
    include/widgets/noise_widget.h \
    include/widgets/envelope_widget.h \
    include/widgets/low_pass_widget.h \
    include/widgets/multiplier_widget.h \
    include/widgets/adder_widget.h \
    include/widgets/visualizer_widget.h \
    include/widgets/patch_widget.h \
    include/widgets/cross_fade_widget.h \
    include/widgets/midi_widget.h \
    include/synth/components/cross_fade.h \
    include/synth/components/lfo.h \
    include/widgets/lfo_widget.h \
    include/widgets/indicator_label.h \
    dep/rtmidi/RtMidi.h

FORMS += \
    forms/window.ui

RESOURCES += \
    res/qdarkstyle/style.qrc

CONFIG += c++11 debug

# Force usage of QT's compiler
win32 {
    QMAKE_CC  = C:/Qt/Tools/mingw530_32/bin/gcc
    QMAKE_CXX = C:/Qt/Tools/mingw530_32/bin/g++
    QMAKE_LINK = C:/Qt/Tools/mingw530_32/bin/g++
}

# Deal with libraries
unix:DEFINES += __LINUX_ALSA__
mac:DEFINES += __LINUX_ALSA__
#win32:DEFINES +=

unix:LIBS += -lasound #-ljack
mac:LIBS += -ljack
#win32:LIBS += c:/mylibs/math.lib
